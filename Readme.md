# Curso Android con kotlin

#TODO definir caso de Estudio

## Sesión 1

### Layouts
* LinearLayout
* ConstraintLayout
* GridViewLayout

### Views
* Button
* ImageButton
* ImageView
* TextView
* EditText
* WebView
* CheckBox
* RadioGroup/RadioButton?
* ToogleButton
* Switch
* FloatingButton

### Navigation
* App Bar
    * Menus
* DrawerLayout
* Bottom Navigation Bar


### Data Binding

* Syntetick KT
* ViewModel
    * Live Data
* Butterknife
* Data binding

## Sesión 2

### Context
* Que es?
* Usos
    * dialogos
    * start activity
    * inflate layout
    * start/bind to service
    * broadcast send/register

### Activity y Fragment
* ciclo de vida activity
    * stack
    * dialog fragment
* ciclo de vida fragment
    * transacciones
    * fragment manager
    * stack

### Intents
* explicitos e implicitos
    * intents usuales
* Comunicacion entre activities
    * Bundles y extras
* Comunicacion con fragmentos
    * Interfaces

### Retrofit, GSOn y consumo de APIs
* @SerializedName
* Service Interface
* Retrofit
* GSON
    * fromJson, toJson

## Sesión 3

### SharedPReferences
* SharedPreferences
    * Editor

### Room and Databases
* sqlite?

### Android Media (Pendiente)
* Media pplayer
    * media from web
* Surface View
* Exo Player

### Adapters
* Patron ViewHolder
* REcycler View

### Maps Y FusedLocationProvider (Pendiente)
* maps API
    * markers

## Sesión 4



### Animations
* Frame
* Fadding

### Material Desing ,estilos y temas
* Temas
* estilos
* fonts
* colors.xml
* selectores
* shapes
    * gradient
    * corners
    * solid
* dimens.xml
    * px
    * in
    * mm
    * pt
    * dp
    * sp 
* anim
    * alpha
    * set
    * translate

### Notificaciones
* Notificaciones channel
* Notificaciones con accion 
    * TAp
    * button

## Sesión 5

### Work Manager
* Workers

### Permisos
* Manifiesto
* Requesting Permisions
    * The kotlin Way

### Concurrencia
* Async Task
* Looper y Main Thread
* Handlers
    * ThreadPools
    * Executor
    * Task Executor
* Corrutinas
    * Dispatcher y BUilder
    * Suspending functions

### Services
* Foreground
* Background
* Bound
* Service
    * Ciclo de Vida
    * Service Class
    * Declarando Servicio
    * Iniciar y Detener el servicio

## Sesión 

### Mensajes y broadcast
* Intent Broadcast
* Broadcast Receiver
* Local BroadcasrManager
* Broadcast Message

### ContentProviders



### Content Loader


### High performance
* Optimizacion de Layout
    * include
    * merge
* Optimizar Bateria
* Responsividad

## Sesión 7

### Hilt (Inyeccion de dependencias)/ Dagger
* DAgger
* Hilt

### Paging Library
* DataSource

### Testing
* Junit
    * Suite
* Espresso

## Sesión 8

### Deploy
* Ofuscar
* Firmar la app
* publicar a tienda

### Firebase
* Firestore
* ML Kit (Pendiente)

### RXJava/RxKotlin

### Flow