package mx.chemasmas.cursos.myanimations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import mx.chemasmas.cursos.myanimations.databinding.ActivityMain2Binding
import mx.chemasmas.cursos.myanimations.services.CardService
import mx.chemasmas.cursos.myanimations.services.CardServiceImpl
import mx.chemasmas.cursos.myanimations.viewmodel.BaseViewModel

class MainActivity2 : AppCompatActivity(),
    CardService by CardServiceImpl()
{

    val modelo: BaseViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main2)
        val binding: ActivityMain2Binding= DataBindingUtil.setContentView(this,R.layout.activity_main2)
        binding.lifecycleOwner = this

        observers()
    }

    private fun observers() {
        modelo.draw.observe(this){
            Log.d("",it.toString())
        }
    }

    fun nuevoDeck(view: View){
        nuevoDeck(modelo)
    }

    fun jalarCarta(view: View){
        drawOneCard(
            modelo.deck.value?.deck_id ?: ""
            ,modelo)
    }

    companion object{
        @JvmStatic
        @BindingAdapter("cardImage")
        fun loadImage(view: ImageView, url:String?){

            url?.let{
                Glide.with(view.context).load(url).into(view)
            }

        }
    }
}