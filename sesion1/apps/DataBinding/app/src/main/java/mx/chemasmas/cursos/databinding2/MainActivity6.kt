package mx.chemasmas.cursos.databinding2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.databinding2.databinding.ActivityMain6BindingImpl
import mx.chemasmas.cursos.databinding2.flow.CardDataSource
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel
import java.io.IOException



class MainActivity6 : AppCompatActivity() {

    val manoT:MutableLiveData<ArrayList<Pair<String,String>>> = MutableLiveData()

    val model:BaseViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main6)
        val binding: ActivityMain6BindingImpl = DataBindingUtil.setContentView(this,R.layout.activity_main6)
        binding.lifecycleOwner = this
        binding.mano = model

    }

    fun initiateFlow(v: View){
        GlobalScope.launch {
            val flow1 = CardDataSource("7y6o80o8ead0").twoCard
                .filter{
                    it.size == 2
                }
                .onCompletion {
                    Log.d("flow","Flujo 1 Completado")
                }
                .collect {
                    model.texas.postValue(it)
                }
//            val flow2 = CardDataSource("7y6o80o8ead0").tenCards.map {
//                    it -> it.map { it2 -> Pair(it2.code,it2.image) }
//            }.filter {
//                it.isNotEmpty()
//            }
//                .onEach {
//                    val cartaG = it.any { it2-> it2.first.startsWith("A") }
//                    if(cartaG){
//                        throw IOException("Aparecio un AS en el flow")
//                    }
////
//                }
//                .catch { ex ->
//                    Log.d("flow",ex.toString())
//                }
//                .onCompletion {
//                    Log.d("flow","Flujo 2 Completado")
//                }
//                .collect {
//                    model.flowHand.postValue(it)
//                    Log.d("flow",it.toString())
//                }

            delay(1000)
        }
    }
}