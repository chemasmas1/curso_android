package mx.chemasmas.cursos.myanimations.services

import mx.chemasmas.cursos.myanimations.models.Deck
import mx.chemasmas.cursos.myanimations.models.Draw
import mx.chemasmas.cursos.myanimations.models.GameStatus
import mx.chemasmas.cursos.myanimations.models.PileInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CardsAPI {

    @GET("/api/deck/new/shuffle")
    fun getNewDeck(
        @Query("deck_count") count:Int = 1
//    ):Call<Response<Deck>>
    ):Call<Deck>

    @GET("/api/deck/{deck_id}/draw/")
    fun drawNCards(
        @Path("deck_id") deck_id:String,
        @Query("count") count:Int = 1
    ):Call<Draw>

    @GET("/api/deck/{deck_id}/pile/{pile_name}/add/")
    fun addToPile(
        @Path("deck_id") deck_id:String,
        @Path("pile_name") pile_name:String,
        @Query("cards") cards:String
    ):Call<PileInfo>

    @GET("/api/deck/{deck_id}/pile/{pile_name}/list/")
    fun gameStatus(
        @Path("deck_id") deck_id:String,
        @Path("pile_name") pile_name:String,
    ):Call<GameStatus>
}