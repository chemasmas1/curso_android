package mx.chemasmas.cursos.firebasepracticas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityMain3Binding

class MainActivity3 : AppCompatActivity() {

    companion object{
        const val TAG = "Remote"
    }

    private lateinit var remoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main3)

        val binding:ActivityMain3Binding =  DataBindingUtil.setContentView(this,R.layout.activity_main3)

        binding.preview.setOnClickListener {
            Intent(this,PreviewActivity::class.java).apply {
                startActivity(this)
            }
        }

        binding.analisis.setOnClickListener {
            Intent(this,AnalisisActivity::class.java).apply {
                startActivity(this)
            }
        }

        binding.captura.setOnClickListener {
            Intent(this,CapturaActivity::class.java).apply {
                startActivity(this)
            }
        }

//        remoteConfig = Firebase.remoteConfig
//        val configSettings = remoteConfigSettings {
//            minimumFetchIntervalInSeconds = 10
////            minimumFetchIntervalInSeconds = 3600
//        }
//        remoteConfig.setConfigSettingsAsync(configSettings)
//
        Firebase.remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                val updated = task.result
                Log.d(TAG, "Config params updated: $updated")
                Toast.makeText(this, "Fetch and activate succeeded",
                    Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Fetch failed",
                    Toast.LENGTH_SHORT).show()
            }
            displayWelcomeMessage()
        }

//        displayWelcomeMessage()
    }

    private fun displayWelcomeMessage() {
        Log.d(TAG,Firebase.remoteConfig.all.toString())
        Log.d(TAG,Firebase.remoteConfig.getString( "saludo" ) )
        Log.d(TAG,Firebase.remoteConfig.getBoolean( "saludo_caps" ).toString() )
        Log.d(TAG,Firebase.remoteConfig.getLong( "long_number" ).toString() )


    }
}