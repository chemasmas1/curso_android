package mx.chemasmas.cursos.myanimations.models

import java.io.Serializable
import androidx.annotation.Keep
import com.google.gson.JsonObject
import org.json.JSONObject

@Keep
data class PileInfo(
    val success: Boolean, // true
    val deck_id: String, // 3p40paa87x90
    val remaining: Int, // 12
    val piles: JsonObject
) : Serializable