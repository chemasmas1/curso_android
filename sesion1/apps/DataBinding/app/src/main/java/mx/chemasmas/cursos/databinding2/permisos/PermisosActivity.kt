package mx.chemasmas.cursos.databinding2.permisos

import android.content.pm.PackageManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.atomic.AtomicInteger

open class PermisosActivity:AppCompatActivity() {
    val tag = "permisos"
    val contador = AtomicInteger()
    val permisosReq = ConcurrentHashMap<Int,List<String>>()
    val permisosCall = ConcurrentHashMap<List<String>,CallbackPermisos>()


    val defaultCallback = object:CallbackPermisos{
        override fun onPermisosExitosos(permisos: List<String>) {
            Log.i(tag, "Permission granted [ $permisos ]")
        }

        override fun onPermisosFallo(permisos: List<String>) {
            Log.e(tag, "Permission denied [ $permisos ]")
        }
    }

    fun requesPermisos(
        vararg permisos: String,
        callbackPermisos: CallbackPermisos = defaultCallback
    ){
        val id = contador.incrementAndGet()
        val items = mutableListOf<String>()
        items.addAll(permisos)
        permisosReq[id] = items
        permisosCall[items] = callbackPermisos
        ActivityCompat.requestPermissions(this,permisos,id)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        val items = permisosReq[requestCode]
        items?.let{
            var exito = true
            for(x in 0..grantResults.lastIndex){
                val result = grantResults[x]
                if( result != PackageManager.PERMISSION_GRANTED )
                {
                    exito = false
                    break
                }
            }
            val callback = permisosCall[items]
            if(exito){
                callback?.onPermisosExitosos(items)
            }else{
                callback?.onPermisosFallo(items)
            }
        }
    }
}