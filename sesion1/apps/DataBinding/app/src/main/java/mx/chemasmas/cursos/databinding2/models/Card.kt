package mx.chemasmas.cursos.databinding2.models

/*
"image": "https://deckofcardsapi.com/static/img/KH.png",
            "value": "KING",
            "suit": "HEARTS",
            "code": "KH"
 */
data class Card(val image:String,val value:String,val suit:String,val code:String)
