package mx.chemasmas.cursos.myanimations.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import mx.chemasmas.cursos.myanimations.models.Deck
import mx.chemasmas.cursos.myanimations.models.Draw
import mx.chemasmas.cursos.myanimations.models.GameStatus
import mx.chemasmas.cursos.myanimations.models.PileInfo
import mx.chemasmas.cursos.myanimations.room.Hand

class BaseViewModel: ViewModel() {
    val anterior: MutableLiveData<Hand> = MutableLiveData()
    val status: MutableLiveData<GameStatus> = MutableLiveData()
    val username: MutableLiveData<String> = MutableLiveData()
    val hand: MutableLiveData<PileInfo> = MutableLiveData()
    val draw: MutableLiveData<Draw> = MutableLiveData()
    val deck: MutableLiveData<Deck> = MutableLiveData()
}