package mx.chemasmas.cursos.myanimations.customui

import android.R.attr
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import kotlin.math.abs


class MiCanvas @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    private var mH = 0
    private var mW  = 0
    private val TOUCH_TOLERANCE = 4f
    private var my: Float = 0f
    private var mx: Float = 0f
    private var mCanvas: Canvas? = null
    private var mBitmap: Bitmap? = null
    private var mPaint: Paint
    private var mPath: Path
    private var mDrawColor: Int = ResourcesCompat.getColor(
        resources,
        android.R.color.holo_orange_dark, null
    )

    init {

        val backgroundColor: Int = ResourcesCompat.getColor(
            resources,
            android.R.color.holo_green_light, null
        )


        mPath = Path()

        mPaint = Paint()
        mPaint.color = backgroundColor

        mPaint.isAntiAlias = true

        mPaint.isDither = true
        mPaint.style = Paint.Style.STROKE // default: FILL

        mPaint.strokeJoin = Paint.Join.ROUND // default: MITER

        mPaint.strokeCap = Paint.Cap.ROUND // default: BUTT

        mPaint.strokeWidth = 12f // default: Hairline-width (really thin)

    }

    override fun onSizeChanged(
        width: Int, height: Int,
        oldWidth: Int, oldHeight: Int
    ) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        mW = width
        mH = height
        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mCanvas = Canvas(mBitmap!!)
        mCanvas?.drawColor(mDrawColor) // Llenamos el canvas de color
        
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawBitmap(mBitmap!!, 0f, 0f, mPaint)
        canvas.drawPath(mPath, mPaint)


    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchStart(x, y)
            MotionEvent.ACTION_MOVE -> {
                touchMove(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                touchUp()
                invalidate()
            }
            else -> {
            }
        }
        return true
    }

    private fun touchUp() {
//        mPath.reset()

    }

    private fun touchMove(x: Float, y: Float) {
//        mPath.lineTo(x, y)

        val dx: Float = abs(x - mx)
        val dy: Float = abs(y - my)
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            // QuadTo() adds a quadratic bezier from the last point,
            // approaching control point (x1,y1), and ending at (x2,y2).
            mPath.quadTo(mx, my, (x + mx) / 2, (y + my) / 2)
            mx = x.toFloat()
            my = y.toFloat()
            // Draw the path in the extra bitmap to save it.
            mCanvas?.drawPath(mPath, mPaint)
        }
    }



    private fun touchStart(x: Float, y: Float) {
        mPath.moveTo(x, y)
        mx = x
        my = y
    }

    fun limpiar(){
        mPath.reset()
        mCanvas?.drawColor(mDrawColor)
        invalidate()
    }


    fun rotar(){
        mCanvas?.save()

        mCanvas?.rotate(45f, (mW/2).toFloat(), (mH/2).toFloat())
        mCanvas?.drawPath(mPath,mPaint)
        mCanvas?.restore()
        invalidate()

    }
}