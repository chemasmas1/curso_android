package mx.chemasmas.cursos.firebasepracticas.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.chemasmas.cursos.firebasepracticas.Mensaje
import mx.chemasmas.cursos.firebasepracticas.R

class MensajesAdapter: RecyclerView.Adapter<MensajesAdapter.ViewHolder>() {

    val lista = arrayListOf<Mensaje>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val display: TextView = itemView.findViewById(R.id.display)
        val msj: TextView = itemView.findViewById(R.id.item_mensaje)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.item_chat,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
         lista
        val item = lista[position]

        with(holder){
            display.text = item.display
            msj.text = item.msj
        }
    }

    override fun getItemCount(): Int = lista.size

    fun showMessages(parsed: HashMap<String, HashMap<String,String>>){
        val res = parsed.entries.map { it.value }
        .map {
            Mensaje(it["id"]!!,it["display"],it["email"],it["msj"]!!)
        }
        lista.clear()
        lista.addAll(res)

    }

    fun addMsj(parsed: HashMap<String,String>) {
        val res = Mensaje(parsed["id"]!!,parsed["display"],parsed["email"],parsed["msj"]!!)
        lista.add(res)
    }
}