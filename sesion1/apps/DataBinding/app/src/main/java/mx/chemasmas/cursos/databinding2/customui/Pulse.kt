package mx.chemasmas.cursos.databinding2.customui

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import mx.chemasmas.cursos.databinding2.R
import kotlin.math.min

class Pulse @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var mY:Float  = 0f
    private var mX: Float = 0f
    private var mRadius: Float = 0f
    private val mPaint: Paint = Paint()
    private val mPulseAnimatorSet = AnimatorSet()
    private val COLOR_ADJUSTER = 5

    init {

        mPaint.color = Color.BLUE


    }

    fun setRadius(radius: Float) {
        mRadius = radius
        mPaint.color =  when(radius.toInt() % 10){
            0 -> Color.rgb(125,65,125)
            1 -> Color.rgb(125,85,125)
            2 -> Color.rgb(125,105,125)
            3 -> Color.rgb(125,125,125)
            4 -> Color.rgb(125,145,125)
            5 -> Color.rgb(125,165,125)
            6 -> Color.rgb(125,185,125)
            7 -> Color.rgb(125,205,125)
            8 -> Color.rgb(125,225,125)
            9 -> Color.rgb(125,245,125)
            else -> Color.BLACK
        }
//        mPaint.color = Color.GREEN + radius.toInt() / COLOR_ADJUSTER
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event!!.actionMasked == MotionEvent.ACTION_DOWN) {
            mX = event.x
            mY = event.y
            if(mPulseAnimatorSet != null && mPulseAnimatorSet.isRunning){
                mPulseAnimatorSet.cancel()
            }
            mPulseAnimatorSet.start()
        }

        return super.onTouchEvent(event)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mPulseAnimatorSet.play(
            AnimatorInflater.loadAnimator(rootView.context, R.animator.anim2)
        )
        mPulseAnimatorSet.setTarget(this)



//        mY = h /2f
//        mX = w /2f


//        val growAnimator = ObjectAnimator.ofFloat(
//            this,
//            "radius", 0f, width.toFloat() * .2f
//        )
//        growAnimator.duration = 2000L
////        growAnimator.interpolator = LinearInterpolator()
//        growAnimator.interpolator = LinearOutSlowInInterpolator()
//
//        val shrinkAnimator = ObjectAnimator.ofFloat(
//            this,"radius",width.toFloat() * .2f,0f
//        )
//        shrinkAnimator.duration = 2000L
//        shrinkAnimator.interpolator = LinearInterpolator()
//        shrinkAnimator.startDelay = 1000L
//
//        val repeatAnimator = ObjectAnimator.ofFloat(
//            this,
//            "radius",
//            0f, width.toFloat() * .2f
//        )
//
//        repeatAnimator.duration = 2000L
//        repeatAnimator.repeatCount = 5
//        repeatAnimator.repeatMode = ValueAnimator.REVERSE
//
//        mPulseAnimatorSet.play(growAnimator).before(shrinkAnimator)
//        mPulseAnimatorSet.play(repeatAnimator).after(shrinkAnimator)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

//        canvas?.drawColor(Color.CYAN)
//        canvas?.drawCircle(mX, mY, mRadius, mPaint)
        canvas?.drawCircle(mX, mY, mRadius, mPaint)
    }

}