package mx.chemasmas.cursos.firebasepracticas

import android.graphics.BitmapFactory
import android.icu.text.SimpleDateFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.camera.core.*
import androidx.camera.core.ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityCapturaBinding
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityPreviewBinding
import java.io.File
import java.nio.file.Files
import java.util.*
import java.util.concurrent.Executors

class CapturaActivity : AppCompatActivity() {

    private var imageCapture: ImageCapture? = null
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>
    private lateinit var binding : ActivityCapturaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_captura)

        binding =  DataBindingUtil.setContentView(this,R.layout.activity_captura)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(this))

        binding.floatingActionButton.setOnClickListener {
            onClick()
        }

    }

    private fun bindPreview(cameraProvider : ProcessCameraProvider) {
        var preview : Preview = Preview.Builder()
            .build()

        var cameraSelector : CameraSelector = CameraSelector.Builder()
//            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()


        preview.setSurfaceProvider(binding.previewCaptura.surfaceProvider)

//        var camera = cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, preview)

          imageCapture = ImageCapture.Builder()
            .setTargetRotation(binding.previewCaptura.display.rotation)
              .setCaptureMode(CAPTURE_MODE_MINIMIZE_LATENCY)
            .build()

        cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, imageCapture, preview)


    }

    fun onClick() {
        val imageCapture = imageCapture ?: return
//
//        val photoFile = File(
//            applicationContext.filesDir,
//            System.currentTimeMillis().toString() + ".jpg")

        imageCapture.takePicture(
            ContextCompat.getMainExecutor(this),
            object: ImageCapture.OnImageCapturedCallback() {
                override fun onCaptureSuccess(image: ImageProxy) {
                    // Use the image, then make sure to close it.
                    binding.imageView.setImageBitmap(
                        binding.previewCaptura.bitmap
                    )
                    image.close()
                }

                override fun onError(exception: ImageCaptureException) {
                    val errorType = exception.getImageCaptureError()

                }
            }
        )

//        val photoFile = File.createTempFile(System.currentTimeMillis().toString(),null)
//
//        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(
//            photoFile
//        ).build()
//        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this),
//            object : ImageCapture.OnImageSavedCallback {
//                override fun onError(error: ImageCaptureException)
//                {
//                    // insert your code here.
//                }
//                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
//                    Log.d("captura",outputFileResults.savedUri.toString())
//                }
//            })
    }
}