package mx.chemasmas.cursos.firebasepracticas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Size
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityAnalisisBinding

class AnalisisActivity : AppCompatActivity() {
    private lateinit var imageAnalysis: ImageAnalysis
    private lateinit var cameraProvider: ProcessCameraProvider
    private lateinit var binding: ActivityAnalisisBinding
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_analisis)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_analisis)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(this))

        binding.previewAnalisis.setOnClickListener {
            click()
        }


    }

    private fun bindPreview(cameraProvider : ProcessCameraProvider) {
        var preview : Preview = Preview.Builder()
            .build()
        var cameraSelector : CameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
//            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()

        imageAnalysis = ImageAnalysis.Builder()
            .setTargetResolution(Size(1280, 720))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()
//
//
        preview.setSurfaceProvider(binding.previewAnalisis.surfaceProvider)
//
        var camera = cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, preview,imageAnalysis)


    }

    fun click(){
//        val imageAnalysis = ImageAnalysis.Builder()
//            .setTargetResolution(Size(1280, 720))
//            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
//            .build()
//
//        var cameraSelector : CameraSelector = CameraSelector.Builder()
//            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
////            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
//            .build()

        val executor = ContextCompat.getMainExecutor(this)
//        imageAnalysis.setAnalyzer(executor, ImageAnalysis.Analyzer { image ->
//            val rotationDegrees = image.imageInfo.rotationDegrees
//            // insert your code here.
//        })
        imageAnalysis.setAnalyzer(executor,YourImageAnalyzer(this))

//        cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, imageAnalysis)
    }

}

private class YourImageAnalyzer(val analisisActivity: AnalisisActivity) : ImageAnalysis.Analyzer {

    val recognizer = TextRecognition.getClient()

    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image
        if (mediaImage != null) {
            val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
            val result = recognizer.process(image)
                .addOnSuccessListener { visionText ->
                    Log.d("texto",visionText.toString())
                    Log.d("texto",visionText.text)
                    analisisActivity.finish()
                }
                .addOnFailureListener { e ->
                    // Task failed with an exception
                    // ...
                    Log.d("texto",e.message.toString())
                    analisisActivity.finish()
                }
        }
    }
}