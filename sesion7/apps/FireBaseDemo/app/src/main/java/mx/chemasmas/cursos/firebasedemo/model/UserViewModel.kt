package mx.chemasmas.cursos.firebasedemo.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser


class UserViewModel: ViewModel() {

    val gUser: MutableLiveData<FirebaseUser> = MutableLiveData()
    val fatal: MutableLiveData<Throwable> = MutableLiveData()
}