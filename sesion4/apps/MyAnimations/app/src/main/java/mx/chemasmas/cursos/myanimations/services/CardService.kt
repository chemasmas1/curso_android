package mx.chemasmas.cursos.myanimations.services

import mx.chemasmas.cursos.myanimations.models.Draw
import mx.chemasmas.cursos.myanimations.viewmodel.BaseViewModel

interface CardService {
    fun nuevoDeck(modelo: BaseViewModel)
    fun drawOneCard(id_deck:String,modelo: BaseViewModel?)
    fun drawOneCard(id_deck:String): Draw?
    fun addToHand(id_deck:String,pileName:String,card:String,modelo: BaseViewModel)
    fun getStatus(id_deck:String,pileName:String,modelo: BaseViewModel)
}