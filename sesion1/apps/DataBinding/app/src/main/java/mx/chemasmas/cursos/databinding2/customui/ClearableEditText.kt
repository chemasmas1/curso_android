package mx.chemasmas.cursos.databinding2.customui

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.addTextChangedListener


class ClearableEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = com.google.android.material.R.attr.editTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr) {

    private var clearButtonImage: Drawable = ResourcesCompat.getDrawable(resources,android.R.drawable.ic_menu_close_clear_cancel,null)!!

//    constructor(context:Context):super(context)
//    constructor(context: Context,attrs: AttributeSet):super(context,attrs)
//    constructor(context: Context,attrs: AttributeSet,defStyleAttr: Int):super(context,attrs,defStyleAttr)


    init{
        setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,null,null,null
        )

        addTextChangedListener(
            { _,_,_,_ -> },
            { texto,_,_,_ ->
                if(texto.isNullOrEmpty())
                    hideClear()
                else
                    showClear()
            },
            {}
        )

        setOnTouchListener { _, event ->
            var clicked = false
            val drawableContainer =  compoundDrawablesRelative[2]
            if( drawableContainer != null){
                val clearStart = width - paddingEnd - clearButtonImage?.intrinsicWidth
                if( event.x > clearStart){
                    clicked = true
                }
            }
            if(clicked){
                when(event.action){
                    MotionEvent.ACTION_DOWN -> showClear()
                    MotionEvent.ACTION_UP -> {
                        text?.clear()
                        hideClear()
                    }
                }
            }


            false
        }
    }

    private fun hideClear() {
        setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,null,null,null
        )
    }

    private fun showClear() {
        setCompoundDrawablesRelativeWithIntrinsicBounds(
            null,null,clearButtonImage,null
        )
    }
}