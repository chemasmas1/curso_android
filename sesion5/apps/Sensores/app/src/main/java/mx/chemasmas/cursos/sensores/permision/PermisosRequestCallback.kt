package mx.chemasmas.cursos.sensores.permision

/*
Permisos al estilo kotlin

 */
interface PermisosRequestCallback {
    fun onPermisionGranted( permisos:List<String>)
    fun onPermisionDenied( permisos:List<String>)
}