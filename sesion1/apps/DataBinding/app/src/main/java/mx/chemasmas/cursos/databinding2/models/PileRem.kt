package mx.chemasmas.cursos.databinding2.models

import java.io.Serializable
import androidx.annotation.Keep

@Keep
data class PileRem(
    val remaining: Int // 2
) : Serializable