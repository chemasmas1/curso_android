package mx.chemasmas.cursos.sesion8

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.View
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.snackbar.Snackbar
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import mx.chemasmas.cursos.sesion8.databinding.ActivityAnalisisBinding
import mx.chemasmas.cursos.sesion8.databinding.ActivityPreviewBinding
import java.util.concurrent.Executors

class AnalisisActivity : AppCompatActivity() {

    private lateinit var imageAnalysis: ImageAnalysis
    lateinit var binding:ActivityAnalisisBinding
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_analisis)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_analisis)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()
                vincular(cameraProvider)
            },
            ContextCompat.getMainExecutor(this)
        )
    }

    @SuppressLint("RestrictedApi")
    private fun vincular(cameraProvider: ProcessCameraProvider?) {
        var preview = Preview.Builder()
            .setBackgroundExecutor(
                Executors.newSingleThreadExecutor()
            )
            .build()

        val camerSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()

        preview.setSurfaceProvider(
            binding.previewView.surfaceProvider
        )

        imageAnalysis = ImageAnalysis.Builder()
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .setTargetResolution(Size(1280, 720))
            .build()

        val camera = cameraProvider?.bindToLifecycle(this as LifecycleOwner,camerSelector,preview,imageAnalysis)
    }

    fun click(v: View){
//        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(this), ImageAnalysis.Analyzer { image ->
//            val rotationDegrees = image.imageInfo.rotationDegrees
//
//            // insert your code here.
//        })

        imageAnalysis.setAnalyzer(
            ContextCompat.getMainExecutor(this),
            MyTextRecognition(binding.root)
        )
    }

}

class MyTextRecognition(val root: View) :ImageAnalysis.Analyzer{

    val recognizer = TextRecognition.getClient()

    override fun analyze(proxy: ImageProxy) {
        val image = proxy.image
        image ?: return

        val input = InputImage.fromMediaImage(image,proxy.imageInfo.rotationDegrees)

        val result = recognizer.process(input)
            .addOnSuccessListener { texto ->
                Log.d("texto",texto.text)
                Snackbar.make(root,texto.text,Snackbar.LENGTH_LONG).show()
            }
            .addOnFailureListener {e->
                Log.d("texto",e.message.toString())
            }

    }

}