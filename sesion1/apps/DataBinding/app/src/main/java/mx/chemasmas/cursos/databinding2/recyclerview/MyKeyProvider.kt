package mx.chemasmas.cursos.databinding2.recyclerview

import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView

class MyKeyProvider(val adapter:CartasAdapter):ItemKeyProvider<String>(SCOPE_CACHED) {
    override fun getKey(position: Int): String? {
        return adapter.getItem(position).code
    }

    override fun getPosition(key: String): Int {
        return adapter.getPosition(key)
    }
}