package mx.chemasmas.cursos.myanimations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.myanimations.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)  // Para uso con Data Binding

    }

    fun rotarV(v: View){
//        binding.miCanvas.rotar()
    }

    fun limpiar(v: View){
//        binding.miCanvas.limpiar()
    }
}