package mx.chemasmas.cursos.catalogolayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class CatalogoViews : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalogo_views)
    }

    fun goToLayout(v: View){
        val clase = when(v.id){
            R.id.botones-> ButtonActivity::class.java
            R.id.imagen_view-> ImageViewActivity::class.java
            R.id.imagen_botom-> ImageButtonActivity::class.java
            R.id.text_view-> TextViewActivity::class.java
            else -> null
        }
        clase?.let {
            val i = Intent(this,it)
            startActivity(i)
        }
    }
}