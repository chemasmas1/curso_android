package mx.chemasmas.cursos.firebasepracticas

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Rational
import android.view.OrientationEventListener
import android.view.Surface
import androidx.camera.camera2.Camera2Config
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityMain3Binding
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityPreviewBinding

class PreviewActivity : AppCompatActivity()
//    ,CameraXConfig.Provider
{

    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>
    private lateinit var binding : ActivityPreviewBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_preview)
        binding =  DataBindingUtil.setContentView(this,R.layout.activity_preview)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(this))


//        val imageCapture = ImageCapture.Builder()
////            .setFlashMode(...)
////        .setTargetAspectRatio(...)
//        .build()
//
//        val orientationEventListener = object : OrientationEventListener(this as Context) {
//            override fun onOrientationChanged(orientation : Int) {
//                // Monitors orientation values to determine the target rotation value
//                val rotation : Int = when (orientation) {
//                    in 45..134 -> Surface.ROTATION_270
//                    in 135..224 -> Surface.ROTATION_180
//                    in 225..314 -> Surface.ROTATION_90
//                    else -> Surface.ROTATION_0
//                }
//
//                imageCapture.targetRotation = rotation
//            }
//        }
//        orientationEventListener.enable()
//


    }

    private fun bindPreview(cameraProvider : ProcessCameraProvider) {
        var preview : Preview = Preview.Builder()
            .build()

        var cameraSelector : CameraSelector = CameraSelector.Builder()
//            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()


        preview.setSurfaceProvider(binding.previewView.surfaceProvider)

        var camera = cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, preview)
    }

//    override fun getCameraXConfig(): CameraXConfig {
//        return Camera2Config.defaultConfig()
//    }
}