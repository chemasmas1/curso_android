package mx.chemasmas.cursos.databinding2.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import mx.chemasmas.cursos.databinding2.models.*
import mx.chemasmas.cursos.databinding2.room.Hand

class BaseViewModel: ViewModel() {
    val texas: MutableLiveData<ArrayList<Card>> = MutableLiveData()
    val flowHand: MutableLiveData<List<Pair<String,String>>> = MutableLiveData(arrayListOf(
        Pair("",""),Pair("",""),Pair("",""),Pair("",""),Pair("","")
    ))
    val mano:MutableLiveData<ArrayList<Card>> = MutableLiveData(arrayListOf())
    val anterior: MutableLiveData<Hand> = MutableLiveData()
    val status: MutableLiveData<GameStatus> = MutableLiveData()
    val username: MutableLiveData<String> = MutableLiveData()
    val hand: MutableLiveData<PileInfo> = MutableLiveData()
    val draw: MutableLiveData<Draw> = MutableLiveData()
    val deck: MutableLiveData<Deck> = MutableLiveData()

}