# Firebase 

## Realtime Database
La informacion se sincroniza con todos los clientes en tiempo real y los mantiene disponibles cuando la app no tiene conexion

## Funciones Clave
* Tiempo Real: En lugar de solicitudes HTTP tipicas, usa la sincronizacion de datos. Cada que hay un cambio , los clientes reciben una actualizacion

* Sin conexion: La app sigue funcionando sin conexion, manteniendo la informacion en disco y sincronizandola cuando la conexion regresa.

* Acceso directo desde dispositivos cliente sin necesidad de un servidor de aplicaciones.

## Ruta de implementacion

* Integrar el SDK en la app
    ```
    implementation 'com.google.firebase:firebase-database-ktx'
    ```

* Crear referencias de Realtime Database
* Configurar datos y detectar cambios
* Persistencia sin conexion
* Asegurar los datos

## Como se estructuran los datos

Todo se almacena en un objeto JSON . No existen registros , se modifican nodos a un objeto JSON.

## Practicas recomendadas

* Evita los datos excesivamente anidados.
    El maximo de niveles de anidacion en la base de datos es 32.
* Compacta las estructuras de datos.
    Si los datos se dividen en rutas de acceso no normalizadas, se pueden descargar de manera mas eficaz
