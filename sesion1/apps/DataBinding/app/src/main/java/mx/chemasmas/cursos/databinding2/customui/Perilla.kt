package mx.chemasmas.cursos.databinding2.customui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import mx.chemasmas.cursos.databinding2.R
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

class Perilla @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var seleccionado: Int = 0
    private val paintTexto: Paint = Paint()
    private val paintBase: Paint = Paint()
    private val paintBaseEncendida: Paint = Paint()
    private var radio: Float = 0f
    private var mH: Int = 0
    private var mW: Int = 0
    private val mTempResult = FloatArray(2)
    private val mTempLabel = StringBuffer(8)

    private var niveles = 4

    init{
        paintBase.color = Color.GRAY
        paintBaseEncendida.color = Color.GREEN
        paintTexto.color = Color.BLACK
        paintTexto.textAlign = Paint.Align.CENTER
        paintTexto.textSize = 50f

//        seleccionado = 2

        setOnClickListener {
            seleccionado = (seleccionado + 1) % niveles
            invalidate()
        }

        if( attrs != null){
            val typedArray = context.obtainStyledAttributes(
                attrs, R.styleable.Perilla,0,0
            )
            paintBase.color = typedArray.getColor( R.styleable.Perilla_baseApagadaColor,paintBase.color)
            paintBaseEncendida.color = typedArray.getColor( R.styleable.Perilla_baseEncendidoColor,paintBase.color)
            niveles = typedArray.getInt( R.styleable.Perilla_nivelesMax ,niveles )
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
//        super.onSizeChanged(w, h, oldw, oldh)
        mH = h
        mW = w
        radio = (min(mW,mH) / 2).toFloat() * 0.8f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawColor( Color.WHITE )
        if(seleccionado > 0){
            canvas?.drawCircle((mW/2).toFloat(),mH/2f,radio,paintBaseEncendida)
        }else{
            canvas?.drawCircle((mW/2).toFloat(),mH/2f,radio,paintBase)
        }


        val labelRadio = radio + 30
        val label = mTempLabel
        for (i in 0 until niveles){
            val xyData = computeXYForPosition(i, labelRadio)
            val x = xyData!![0]
            val y = xyData!![1]
            label.setLength(0)
            label.append(i)
            canvas?.drawText(label,0,label.length,x,y,paintTexto)
        }

        val selectorRadio = radio - 50
        val xyData = computeXYForPosition(
            seleccionado,
            selectorRadio
        )
        val x = xyData!![0]
        val y = xyData!![1]

        canvas!!.drawCircle(x,y,30f,paintTexto)
    }

    private fun computeXYForPosition(pos: Int, radius: Float): FloatArray? {
        val result = mTempResult
        val startAngle = Math.PI * (9 / 8.0) // Angles are in radians.
        val angle = startAngle + pos * (Math.PI / 4)
        result[0] = (radius * cos(angle)).toFloat() + mW / 2
        result[1] = (radius * sin(angle)).toFloat() + mH / 2
        return result
    }
}