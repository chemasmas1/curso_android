package mx.chemasmas.cursos.databinding2.concurrencia

import android.os.AsyncTask
import kotlinx.coroutines.delay
import mx.chemasmas.cursos.databinding2.MainApplication
import mx.chemasmas.cursos.databinding2.room.Hand
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel

class GetLastCardTask: AsyncTask<Unit, Unit, Hand>() {
    override fun doInBackground(vararg params: Unit?): Hand {
        Thread.sleep(5000)
        return MainApplication.db.handDAO().getLastCardSync()
    }

//    override fun doInBackground(vararg params: BaseViewModel?): Hand {
//        TODO("Not yet implemented")
//    }
}