package mx.chemasmas.cursos.firebasedemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import mx.chemasmas.cursos.firebasedemo.R
import mx.chemasmas.cursos.firebasedemo.model.Mensaje


class MensajeAdapter: RecyclerView.Adapter<MensajeAdapter.ViewHolder>() {
    val lista = arrayListOf<Mensaje>()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val texto: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(
                R.layout.chat,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = lista[position]

        with(holder){
            texto.text = item.msj
        }
    }

    override fun getItemCount(): Int = lista.size

    fun showMessages(parsed: HashMap<String, HashMap<String,String>>){
        val res = parsed.entries.map { it.value }.map {
            Mensaje(it["id"]!!,it["id"]!!,it["id"]!!,it["id"]!!)
        }
        lista.clear()
//        lista.addAll(res)

    }
}