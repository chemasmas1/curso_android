package mx.chemasmas.cursos.databinding2.recyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import mx.chemasmas.cursos.databinding2.BR
import mx.chemasmas.cursos.databinding2.R
import mx.chemasmas.cursos.databinding2.databinding.RvItemCardBinding
import mx.chemasmas.cursos.databinding2.models.Card

class CartasAdapter(val cartas: MutableLiveData<ArrayList<Card>>):RecyclerView.Adapter<CartasAdapter.ViewHolder>() {





//    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
//        val carta:ImageView = itemView.findViewById(R.id.imageView5)
//        val texto:TextView = itemView.findViewById(R.id.textView)
//    }


    var tracker: SelectionTracker<String>? = null
    private var ctx: Context? = null

    inner class ViewHolder(val bind: RvItemCardBinding):RecyclerView.ViewHolder(bind.root){



        fun bindF(item: Card) {
            bind.setVariable(BR.cartaItem,item)

//            bind.imageView5
//            bind.textView
        }

        fun setActivo(pos:Int,isActive:Boolean = true){
            bind.fondoCL.isActivated = isActive
        }

        fun getItemDetails():ItemDetailsLookup.ItemDetails<String> = object :
            ItemDetailsLookup.ItemDetails<String>() {
            override fun getPosition(): Int = adapterPosition

            override fun getSelectionKey(): String? {
                return cartas.value?.get(adapterPosition)?.code
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        ctx = parent.context
        val binding: RvItemCardBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.rv_item_card,
            parent,
            false
        )


//        return ViewHolder(
//            inflater.inflate(
//                R.layout.rv_item_card,
//                parent,
//                false
//            )
//        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = cartas.value!![position]

        with(holder){
//            Glide.with(itemView.context).load(item.image).into(this.bind.imageView5)
            bindF(item)
            bind.fondoCL.clicks().subscribe {
                tracker?.let{
                    setActivo(position,!it.isSelected(item.code))
//                    if(!it.isSelected(item.code)){
//
//                    }
                }
            }

        }

    }

    override fun getItemCount(): Int = cartas.value?.size ?: 0
    fun getItem(position: Int): Card {
        return cartas.value!![position]
    }

    fun getPosition(key: String):Int {
        var pos = -1
        val lista = cartas.value?.filterIndexed{
            i,carta ->
            pos = i
            carta.code == key
        }
        return pos
    }

}