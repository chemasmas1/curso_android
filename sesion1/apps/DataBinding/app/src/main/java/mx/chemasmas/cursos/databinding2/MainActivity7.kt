package mx.chemasmas.cursos.databinding2

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity7 : AppCompatActivity(), SensorEventListener
{
    private var luz: Sensor? = null
    private lateinit var sensorManager: SensorManager
    private var magnetico: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main7)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val lista = sensorManager.getSensorList( Sensor.TYPE_ALL )

        Log.d("sensores",lista.joinToString(",","[","]"))

        magnetico = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        luz = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
//        luz = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.values?.joinToString(",","[","]").let {
            Log.d("sensores","${event?.sensor} $it")
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onResume() {
        super.onResume()
//        magnetico?.let {
//            sensorManager.registerListener(this,it,SensorManager.SENSOR_DELAY_NORMAL)
//        }
        luz?.let {
            sensorManager.registerListener(this,it,SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onPause() {
        super.onPause()
//        magnetico?.let {
//            sensorManager.unregisterListener(this)
//        }
        luz?.let {
            sensorManager.unregisterListener(this)
        }
    }
}