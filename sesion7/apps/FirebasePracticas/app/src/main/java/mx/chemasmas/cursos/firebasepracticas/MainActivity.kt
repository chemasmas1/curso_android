package mx.chemasmas.cursos.firebasepracticas

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import mx.chemasmas.cursos.firebasepracticas.interfaces.GoogleLogin
import mx.chemasmas.cursos.firebasepracticas.interfaces.GoogleLoginImpl
import mx.chemasmas.cursos.firebasepracticas.viewmodel.UserViewModel
import androidx.databinding.DataBindingUtil
import com.google.firebase.auth.FirebaseAuth
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityMainBinding
import mx.chemasmas.cursos.firebasepracticas.interfaces.FacebookLogin
import mx.chemasmas.cursos.firebasepracticas.interfaces.FacebookLoginImpl
import java.security.MessageDigest


class MainActivity : AppCompatActivity(),
GoogleLogin by GoogleLoginImpl(),
FacebookLogin by FacebookLoginImpl()
{

    lateinit var binding:ActivityMainBinding
    val model: UserViewModel by viewModels()
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        create(model,getString(R.string.default_web_client_id),this)
        create(model,binding.loginButton,this)

        checkUser()
        binding.signInButton.setOnClickListener {
            initGLogin()
        }

//        binding.mail.setOnClickListener {
//            mAuth = FirebaseAuth.getInstance()
//            val currentUser = mAuth!!.currentUser
//            if(currentUser == null){
//                Log.d("Login","No hay nadie")
//            }
//            else{
//                mAuth!!.sendPasswordResetEmail(currentUser.email!!).addOnSuccessListener {
//                    Log.d("Login","Enviado")
//                }.addOnFailureListener {
//                    Log.d("Login",it.message.toString())
//                }
//            }
//        }

        model.gUser.observe(this){
            it ?: return@observe
            Log.d("Login",it.toString())
            checkUser()
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            hashThis()
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun hashThis() {

        val info = packageManager.getPackageInfo(
            "mx.chemasmas.cursos.firebasepracticas",
            PackageManager.GET_SIGNING_CERTIFICATES
        )


        for (signature in info.signingInfo.apkContentsSigners) {
            //for (signature in info.signatures) {
            val md: MessageDigest = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
        }

    }

    private fun checkUser() {
        mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth!!.currentUser
        if(currentUser == null){
            Log.d("Login","No hay nadie")
        }
        else{
            Log.d("Login","${currentUser.providerId} ${currentUser.email}")
            Intent(this,MainActivity3::class.java).apply {
//            Intent(this,MainActivity2::class.java).apply {
                startActivity(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activityGoogleResult(requestCode, resultCode, data)
        activityFacebookResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}