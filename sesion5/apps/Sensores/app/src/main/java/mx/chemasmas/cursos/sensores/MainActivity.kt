package mx.chemasmas.cursos.sensores

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View

class MainActivity : AppCompatActivity(), SensorEventListener {

    private var magnetico: Sensor? = null
    private var luz: Sensor? = null
    private var pasos: Sensor?  = null
    private lateinit var sensorManager: SensorManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val deviceSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)

        Log.d("sensores",deviceSensors.toString())

        //sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

        pasos = sensorManager.getSensorList(Sensor.TYPE_STEP_COUNTER).firstOrNull()
        luz = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        magnetico = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)


    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.values?.joinToString(",")?.let { Log.d("event", "${event.sensor} $it") }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onResume() {
        super.onResume()
        pasos?.also {
            sensorManager.registerListener(this,it,SensorManager.SENSOR_DELAY_NORMAL)
        }
        luz?.also {
            sensorManager.registerListener(this,it,SensorManager.SENSOR_DELAY_NORMAL)
        }
        magnetico?.also {
            sensorManager.registerListener(this,it,SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onPause() {
        super.onPause()
        pasos?.also {
            sensorManager.unregisterListener(this)
        }
        luz?.also {
            sensorManager.unregisterListener(this)
        }
        magnetico?.also {
            sensorManager.unregisterListener(this)
        }
    }

    fun irALocation( v: View){
        Intent(this,MainActivity2::class.java).also {
            startActivity(it)
        }
    }
}