package mx.chemasmas.cursos.databinding2

import android.app.*
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.provider.ContactsContract.Directory.PACKAGE_NAME
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.BADGE_ICON_LARGE
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.databinding2.models.Deck
import mx.chemasmas.cursos.databinding2.room.Hand
import mx.chemasmas.cursos.databinding2.services.CardService
import mx.chemasmas.cursos.databinding2.services.CardServiceImpl
import mx.chemasmas.cursos.databinding2.util.SharedPrefAcces
import mx.chemasmas.cursos.databinding2.util.sharedPrefs

class MyService : Service(),
CardService by CardServiceImpl()
{

    inner class LocalBinder: Binder(){
        internal val service:MyService
            get() = this@MyService
    }

    private val CHANNEL_ID = "myChannelService"
    private val NOTIFICATION_ID = 12345678
    val localBinder = LocalBinder()
    private val EXTRA_STARTED_FROM_NOTIFICATION: String = PACKAGE_NAME + ".started_from_notification"

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
        Log.d("service", "onStart")
        stopSelf()
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("service", "onCreate")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("service", "onDestroy")
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d("service", "onBind")

        return localBinder
    }

    fun startDrawingCards(){
        startService(Intent(applicationContext, MyService::class.java))
        GlobalScope.launch {
            repeat(10){
                val json  = sharedPrefs()?.getString(SharedPrefAcces.DECK, "{}") ?: "{}"
                val deck:Deck = Deck.fromJson(json)
                Log.d("service", deck.toString())
                delay(2000)

                val draw = drawOneCard(
                    deck.deck_id
                )
                val mNotificationManager =
                    getSystemService(NOTIFICATION_SERVICE) as NotificationManager

                val carta = draw?.cards?.get(0)

                carta?.let {
                    MainApplication.db.handDAO().agregar( Hand(carta.code,carta.image) )
                }


                val builder = NotificationCompat.Builder(this@MyService, "notif")
                    .setContentText("$carta")
//                    .setContentTitle(draw?.cards?.get(0)?.code ?: "No Card")
//                    .setContentTitle(carta?.code ?: "No Card")
                    .setContentTitle("No Card")
//            .setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setBadgeIconType(BADGE_ICON_LARGE)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setWhen(System.currentTimeMillis())
//            .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MIN)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                    // Create the channel for the notification
                    val mChannel = NotificationChannel(
                        CHANNEL_ID,
                        "canalServicio",
                        NotificationManager.IMPORTANCE_NONE
                    )
                    //            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

                    // Set the Notification Channel for the Notification Manager.
                    mNotificationManager.createNotificationChannel(mChannel)
                    builder.setChannelId(CHANNEL_ID) // Channel ID
                }

                mNotificationManager.notify(NOTIFICATION_ID,builder.build())
            }
        }
    }

    override fun onUnbind(intent: Intent?): Boolean {
        startForeground(
            NOTIFICATION_ID,
            getNotification()
        )

        return true
    }

    fun getNotification(): Notification {

        val  intent = Intent(this, MainActivity2::class.java)
        val activityPendingIntent = PendingIntent.getActivity(
            this, 0,
            intent, 0
        )

        val intent2 = Intent(this, MyService::class.java)
//        intent2.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)
        val servicePendingIntent = PendingIntent.getService(
            this,
            9999,
            intent2,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val builder = NotificationCompat.Builder(this, "notif")
            .setContentText("Hola")
            .setContentTitle("Titulo")
//            .setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setBadgeIconType(BADGE_ICON_LARGE)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setWhen(System.currentTimeMillis())
//            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .addAction(

                NotificationCompat.Action(
                    android.R.drawable.btn_plus,
                    "No hago Nada",
                    activityPendingIntent

                )

            )
            .addAction(
                NotificationCompat.Action(
                    android.R.drawable.btn_plus,
                    "Para!!",
                    servicePendingIntent
                )
            )


                    // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            // Create the channel for the notification
            val mChannel = NotificationChannel(
                CHANNEL_ID,
                "canalServicio",
                NotificationManager.IMPORTANCE_NONE
            )
            //            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel)
            builder.setChannelId(CHANNEL_ID) // Channel ID
        }

        return builder.build()
    }

    fun detener() {
        stopForeground(true)
    }
}