package mx.chemasmas.cursos.firebasepracticas.interfaces

import android.app.Activity
import android.content.Intent
import mx.chemasmas.cursos.firebasepracticas.viewmodel.UserViewModel

interface GoogleLogin {
    fun create(model: UserViewModel, defaultWebClient:String, act: Activity)
    fun initGLogin()
    fun firebaseAuthWithGoogle(idToken: String, id: String)
    fun activityGoogleResult(requestCode: Int, resultCode: Int, data: Intent?)
}