package mx.chemasmas.cursos.databinding2

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.databinding2.permisos.CallbackPermisos

import mx.chemasmas.cursos.databinding2.permisos.PermisosActivity

class MainActivity8 : PermisosActivity(), OnMapReadyCallback {

    private lateinit var myMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return

            val aqui = LatLng(
                20.100807,-98.417323
            )
            for (location in locationResult.locations){
                // Update UI with location data
                Log.d("location", location.toString())
                myMap.moveCamera(
                    CameraUpdateFactory.newLatLng(
                        LatLng( location.latitude, location.longitude)
                    )
                )

                myMap.addPolyline(
                    PolylineOptions()
                        .color( getColor(R.color.design_default_color_primary) )
//                        .endCap(
//                            CustomCap(
//                                BitmapDescriptorFactory.fromResource(R.drawable._casco)
//                            )
//                        )
                        .add(
                            aqui,
                            LatLng( location.latitude, location.longitude)
                        )
                )
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main8)
//        val binding:ActivityMain8BindingImpl  = DataBindingUtil.setContentView(this,R.layout.activity_main8)
//
//        binding.lifecycleOwner = this

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        requesPermisos(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            callbackPermisos = object : CallbackPermisos {
                override fun onPermisosExitosos(permisos: List<String>) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                        requesPermisos(
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        )
                    }
                }

                override fun onPermisosFallo(permisos: List<String>) {
                    Log.e(tag, "Permission denied [ $permisos ]")
                    //
                }

            }
        )
//        GlobalScope.launch {
//            if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                    requestPermissions(
//                        arrayOf(
//                            Manifest.permission.ACCESS_COARSE_LOCATION,
//                            Manifest.permission.ACCESS_FINE_LOCATION
//                        ),
//                        9999
//                    )
//                return
//            }

//            getLocation()
//        }

        val mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.map,mapFragment)
            .commit()

        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        GlobalScope.launch {
            repeat(3600 * 8 ){
                fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    Log.d("location", location.toString())
                }

                delay(1000)
            }


        }
    }

    @SuppressLint("MissingPermission")
    fun iniciarPeticionesUbicacion(){
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 1000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        fusedLocationClient.requestLocationUpdates(
            locationRequest,locationCallback, Looper.getMainLooper()
        )
    }

    fun pararPeticionesUbicacion(){
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        iniciarPeticionesUbicacion()
    }

    override fun onPause() {
        super.onPause()
        pararPeticionesUbicacion()
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(mapa: GoogleMap?) {

        val mapStyle = MapStyleOptions.loadRawResourceStyle(this,R.raw.style_map)
        mapa?.setMapStyle(mapStyle)
        val aqui = LatLng(
            20.100807,-98.417323
        )
        mapa?.addMarker(
            MarkerOptions().position(
                aqui
            ).title("Aqui estamos")
        )
        mapa?.moveCamera(
            CameraUpdateFactory.newLatLng(
                aqui
            )
        )
        mapa?.moveCamera(
            CameraUpdateFactory.zoomTo(20f)
        )
        mapa?.isMyLocationEnabled = true
        mapa?.isTrafficEnabled = true
        mapa?.isIndoorEnabled = true

        myMap = mapa!!
    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        var exito = true
//        if(requestCode == 9999){
//            for ( res in grantResults ){
//                if( res != PackageManager.PERMISSION_GRANTED )
//                {
//                    exito = false
//                    break
//                }
//            }
//            if(exito){
//                getLocation()
//            }
//        }
//    }
}