package mx.chemasmas.cursos.sensores

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.sensores.databinding.ActivityMain2BindingImpl
import mx.chemasmas.cursos.sensores.databinding.ActivityMainBindingImpl
import mx.chemasmas.cursos.sensores.permision.PermisionCompatActivity

//class MainActivity2 : AppCompatActivity() {
class MainActivity2 : PermisionCompatActivity(),
    OnMapReadyCallback
{


    private lateinit var myMap: GoogleMap
    private var requestingLocationUpdates: Boolean =true
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main2)

        val binding: ActivityMain2BindingImpl = DataBindingUtil.setContentView(this,R.layout.activity_main2)
        binding.lifecycleOwner = this
//        binding.maps

        requestPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )



        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    // Update UI with location data
                    Log.d("location", location.toString())
                }
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//
//
        GlobalScope.launch {
//            repeat(10){
//
//                fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
//                    // Got last known location. In some rare situations this can be null.
//                    Log.d("location", location.toString())
//                }.addOnCanceledListener {
//                    Log.d("location", "CAncelado")
//                }
//                .addOnFailureListener {
//                    Log.d("location", it.message.toString())
//                }
//                delay(1000)
//            }
        }

        val mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.map,mapFragment)
            .commit()

        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        if (requestingLocationUpdates) startLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        val locationRequest = LocationRequest.create()?.apply {
            interval = 1000
            fastestInterval = 500
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
        fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallback,
            Looper.getMainLooper())
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }



    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }


    @SuppressLint("MissingPermission")
    fun moveToLocation(v: View){
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                    // Got last known location. In some rare situations this can be null.
                    myMap.moveCamera(
                        CameraUpdateFactory.newLatLng(LatLng(location!!.latitude,location.longitude))
                    )
                    myMap.moveCamera(
                        CameraUpdateFactory.zoomTo(15f)
                    )
//                    myMap.addMarker(
//
//                    )
                }.addOnCanceledListener {
                    Log.d("location", "CAncelado")
                }
                .addOnFailureListener {
                    Log.d("location", it.message.toString())
                }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

//        googleMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
        googleMap?.isIndoorEnabled = true
        googleMap?.isTrafficEnabled = true
        googleMap?.isMyLocationEnabled = true

        googleMap?.addMarker(
            MarkerOptions()
                .position(LatLng(0.0, 0.0))
                .title("Marker")
        )
        myMap = googleMap!!
    }

    fun irAPlaces( v: View){
        Intent(this,MainActivity3::class.java).also {
            startActivity(it)
        }
    }
}