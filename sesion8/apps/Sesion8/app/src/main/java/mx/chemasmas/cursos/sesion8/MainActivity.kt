package mx.chemasmas.cursos.sesion8

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import mx.chemasmas.cursos.sesion8.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        Firebase.remoteConfig.setDefaultsAsync(R.xml.remoteconfig_default);

//        Firebase.remoteConfig.fetchAndActivate().addOnSuccessListener {
////            binding.tag.text = Firebase.remoteConfig.getString("saludo")
//        }

        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 5
        }

        binding.preview.setOnClickListener {
            startActivity(Intent(this,PreviewActivity::class.java))
        }

        binding.captura.setOnClickListener {
            startActivity(Intent(this,CapturaActivity::class.java))
        }
        binding.analisis.setOnClickListener {
            startActivity(Intent(this,AnalisisActivity::class.java))
        }


        Firebase.remoteConfig.setConfigSettingsAsync(configSettings)
        .addOnSuccessListener {
//            Log.d("RC",Firebase.remoteConfig.all.toString())
//            Log.d("RC",Firebase.remoteConfig.getString( "saludo" ) )
//            Log.d("RC",Firebase.remoteConfig.getBoolean( "saludo_caps" ).toString() )
//            Log.d("RC",Firebase.remoteConfig.getLong( "long_number" ).toString() )
////            Log.d("RC", Firebase.remoteConfig.getValue("raw").asString() )
//
//            binding.tag.text = Firebase.remoteConfig.getString("saludo")
        }
//        binding.tag.text = Firebase.remoteConfig.getString("saludo")


        Firebase.remoteConfig.fetchAndActivate().addOnSuccessListener {
            Log.d("RC",Firebase.remoteConfig.all.toString())
            Log.d("RC",Firebase.remoteConfig.getString( "saludo" ) )
            Log.d("RC",Firebase.remoteConfig.getBoolean( "saludo_caps" ).toString() )
            Log.d("RC",Firebase.remoteConfig.getLong( "long_number" ).toString() )
            Log.d("RC",Firebase.remoteConfig.getValue( "raw" ).asString())
            binding.tag.text = Firebase.remoteConfig.getString("saludo")
        }

    }
}