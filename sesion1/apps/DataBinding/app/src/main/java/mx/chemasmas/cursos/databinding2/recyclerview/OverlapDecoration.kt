package mx.chemasmas.cursos.databinding2.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class OverlapDecoration():RecyclerView.ItemDecoration() {

    val overlapR = -200
    val overlapT = 120
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position =  parent.getChildAdapterPosition(view)
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(
            0,
            overlapT,
            overlapR,
            0
        )
    }
}