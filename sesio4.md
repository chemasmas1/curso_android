# Advanced Graphics

## Custom Views

Android cuenta con un grna numero de sub clases de View.

Algunas de estas son:

Button
TextView
EditText
ImageView
CheckBox
RadioButton

Podemos extender estas sub clases, o el propio VIew, para generar vistas que podremos usar dentro de nuestros proyectos.

### Ejercicio 1 CustomEditText

Extenderemos EditTextView para agregarle la funcionalidad de un boton de limpiar (x)


Extenderemos la version de AppCompat

androidx.appcompat.widget.AppCompatEditText

Para generar los constructores necesarios por las vistas usaremos la anotacion

@JvmOverloads,
el cual nos ayuda a generar metodos apartir de un unico metodo , variando segun la cantidad de parametros default definidos

Para las view se deben de generar los siguientes constructores

Class(context:Context)  // Unicamente recibe el contexto y genera la vista minima

Class(context:Context,attr:attrs:AttributeSet)

Requerido para usar nuestra vista en los layouts xml

Class(context:Context,attr:attrs:AttributeSet,defStyleAttr:int)

Requerido si se van  a aplicar temas como Material


### Ejercicio Custom View (Fan Controller)

Extender View

SObre escribiendo OnDraw

Usando Attrs en xml
    TyperAtrrs y procesar los styleables
    declare styleable


## Canvas 

Para dibujar en un canvas, se requieren al menos los siguientes elementos

* Un Canvas, La superficie que se enviara al bitmap
* Un Bitmap, el bitmap que el sistema operativo dibujara
* La View asociada al bitmap
* Un Paint con informacion sobre el color y que dibuja formas


Hay varias maneras de manejar animaciones y graficos en 2d

* Drawables
    * Es un Grafico que puede ser dibujado en la pantalla. Es la mejor alternativa para graficos simples, que no requieren cambios dinamicos
* Canvas
    * Superficie 2d en la que podemos estar redibujando regularmente.
* SurfaceView
    * Es otra superficie de dibujo . Su principal proposito es que un hilo secundario este dibujando el contenido de la vista

* Lottie
    * Es una libreria de Airbnb para animaciones, estas se pueden exportar desde Affter Efects usando el plugin de Bodymovin

### Dibujando en un Canvas

