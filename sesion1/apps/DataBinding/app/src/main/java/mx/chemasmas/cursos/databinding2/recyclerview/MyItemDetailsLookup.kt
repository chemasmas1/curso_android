package mx.chemasmas.cursos.databinding2.recyclerview

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView

class MyItemDetailsLookup(val rv:RecyclerView): ItemDetailsLookup<String>() {
    override fun getItemDetails(e: MotionEvent): ItemDetails<String> {
        val child = rv.findChildViewUnder(e.x,e.y)!!
        return (rv.getChildViewHolder(child) as CartasAdapter.ViewHolder).getItemDetails()
    }
}