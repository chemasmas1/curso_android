# Remote Config

Servicio de Nube que nos permite cambiar comportamientos y apariencias dentro de nuestras aplicaciones sin la necessidad de publicar una actualizacion

## Funciones clave

* implementar cambios rapidos en la base de usuario de la app
* Segmentar la apariencia de o comportamientos basados en idioma, pais o segmentos de google analitycs
* Pruebas A/B

## Ruta de implementacion

* Incorporamos la dependencia dentro del proyecto
* Definimos valores predeterminados para los paramatros a setear
* Agregar la logica para recuperar, activar y obtener esos valores
* actualizar los valores 


## Dependencia necesaria

```
implementation 'com.google.firebase:firebase-config'
```

## Politicas y limites

* No se deben hacer actualizaciones que requieran autorizacion del usuario

* No almacenar en remote config, nada confidencial

* No intentes usar remote config para eludir las politicas de la plataforma

* Hay un limite de 2000 parametros maximo en remote config

* se pueden almacenar hasta 300 versiones de la plantilla de parametros, con un tiempo de vida de 90 dias


## Camera X

Nueva biblioteca para el manejo de la camara en android, incluida en jetpack

Ofrece una interface mas limpia a el uso de la camara atravez de Camera2

casos de uso:

* Vistas previas de la camara
* Analisis de Imagenes, permite obtener imagenes de manera eficiente para usos con el ML Kit
* Captura de imagenes

### Permisos necesarios

```xml
<uses-permission android:name="android.permission.CAMERA"></uses-permission>
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"></uses-permission>
```

Funciona en minimo API 21


## ML KIT


## App BUndle

## Dynamic Delivery y Dynamic Features

## Firmando la app


