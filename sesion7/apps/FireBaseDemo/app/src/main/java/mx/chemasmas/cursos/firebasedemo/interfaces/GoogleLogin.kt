package mx.chemasmas.cursos.firebasedemo.interfaces

import android.app.Activity
import android.content.Intent
import mx.chemasmas.cursos.firebasedemo.model.UserViewModel

interface GoogleLogin {


    fun initGLogin()
    fun create(model: UserViewModel, defaultWebClient:String, act: Activity)
    fun start()
    fun firebaseAuthWithGoogle(idToken: String, id: String)
    fun activityResutlt(requestCode: Int, resultCode: Int, data: Intent?)
    fun closeSesion()
}