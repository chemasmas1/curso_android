package mx.chemasmas.cursos.myanimations.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "manos")
data class Hand(
    val code:String,
    val imagen:String
){
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
}