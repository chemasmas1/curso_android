package mx.chemasmas.cursos.sesion8

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Size
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import mx.chemasmas.cursos.sesion8.databinding.ActivityPreviewBinding
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class PreviewActivity : AppCompatActivity() {
    lateinit var binding:ActivityPreviewBinding

    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_preview)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_preview)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()
                vincular(cameraProvider)
            },
            ContextCompat.getMainExecutor(this)
        )
    }

    @SuppressLint("RestrictedApi")
    private fun vincular(cameraProvider: ProcessCameraProvider?) {
        var preview = Preview.Builder()
            .setBackgroundExecutor(
                Executors.newSingleThreadExecutor()
            )
            .build()

        val camerSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()

        preview.setSurfaceProvider(
            binding.previewView.surfaceProvider
        )

        val camera = cameraProvider?.bindToLifecycle(this as LifecycleOwner,camerSelector,preview)
    }
}