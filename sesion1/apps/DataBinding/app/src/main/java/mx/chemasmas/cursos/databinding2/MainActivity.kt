package mx.chemasmas.cursos.databinding2

//import kotlinx.android.synthetic.main.activity_main.*
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.coroutines.*
import mx.chemasmas.cursos.databinding2.concurrencia.AddToHandLooper
import mx.chemasmas.cursos.databinding2.concurrencia.GetLastCardLooper
import mx.chemasmas.cursos.databinding2.concurrencia.GetLastCardTask
import mx.chemasmas.cursos.databinding2.databinding.ActivityMainBinding
import mx.chemasmas.cursos.databinding2.models.Deck
import mx.chemasmas.cursos.databinding2.models.PileRem
import mx.chemasmas.cursos.databinding2.room.Hand
import mx.chemasmas.cursos.databinding2.services.CardService
import mx.chemasmas.cursos.databinding2.services.CardServiceImpl
import mx.chemasmas.cursos.databinding2.util.SharedPrefAcces
import mx.chemasmas.cursos.databinding2.util.editSharedPrefs
import mx.chemasmas.cursos.databinding2.util.sharedPrefs
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel
import kotlin.random.Random

//import com.chemasmas.customcomponentslibrary.Util

class MainActivity : AppCompatActivity() ,
CardService by CardServiceImpl()
{

//    val INTENTRES = 9999
//    private lateinit var binding: MainActivityBinding

    private val CHANNEL_ID = "myChannel"
    private val NOTIFICATION_ID = 12345679

    private lateinit var JUGADOR: String
    val modelo: BaseViewModel by viewModels()
    lateinit var handHandler:AddToHandLooper // = AddToHandLooper(modelo)
    lateinit var lastCardHandler: GetLastCardLooper // = AddToHandLooper(modelo)

    var myService:MyService? = null

    val mServiceConnection:ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as MyService.LocalBinder
            myService = binder.service
            myService?.startDrawingCards()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            myService = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main) // Es Remplazado por la linea a continuacion
//        val binding = ActivityMainBinding.inflate(layoutInflater) // Para uso con View Binding
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)  // Para uso con Data Binding
        binding.lifecycleOwner = this
        handHandler = AddToHandLooper(modelo)
        handHandler.start()
        lastCardHandler = GetLastCardLooper(modelo)
        lastCardHandler.start()
        binding.model = modelo // Se va si solo usamos ViewBiding
//        modelo.deck.observe(this){
//            name.setText( it.deck_id ) // Esta observacion, la esta haciendo ahora el binding y el xml
//            restantes.setText( it.restantes.toString() )
//        }

//        binding.getDeck.clicks().subscribe{
//            nuevoDeck(modelo)
//        }
//        get_deck.clicks().subscribe{
//            nuevoDeck(modelo)
//        }

        binding.getCard.clicks()
            .subscribe{
            drawOneCard(modelo.deck.value?.deck_id ?: "",modelo)
            val builder = NotificationCompat.Builder(this,CHANNEL_ID)
                .setContentText("Desde la activity")
                .setContentTitle("Hola!!!")
                .setColorized(true)
                .setColor(resources.getColor(R.color.design_default_color_secondary_variant))
                .setSmallIcon(android.R.drawable.arrow_up_float)


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                // Create the channel for the notification
                val mChannel = NotificationChannel(
                    CHANNEL_ID,
                    "nuevoCanal",
                    NotificationManager.IMPORTANCE_NONE
                )
                //            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

                // Set the Notification Channel for the Notification Manager.
                mNotificationManager.createNotificationChannel(mChannel)

            }
            with(NotificationManagerCompat.from(this)){
                notify(NOTIFICATION_ID,builder.build())
            }


        }
//        get_card.clicks().subscribe{
//            drawOneCard(name.text.toString(),modelo)
//        }

        binding.addToHand.clicks().subscribe {
            addToHand(
                modelo.deck.value?.deck_id ?: "",
                JUGADOR,
                modelo.draw.value?.cards?.get(0)?.code ?: "",
                modelo
            )
            getStatus(
                modelo.deck.value?.deck_id ?: "",
                JUGADOR,
                modelo
            )
            myService?.detener()
//            MainApplication.db.handDAO().agregar(
//                Hand(modelo.draw.value?.cards?.get(0)?.code!!,
//                    modelo.draw.value?.cards?.get(0)?.image!!)
//            )

//            val task = AddToHandTask()
//            task.execute(Hand(modelo.draw.value?.cards?.get(0)?.code!!, modelo.draw.value?.cards?.get(0)?.image!!))

//            val msg = Message()
//            msg.obj = Hand(modelo.draw.value?.cards?.get(0)?.code!!, modelo.draw.value?.cards?.get(0)?.image!!)
//            handHandler.handler?.sendMessage(
//                    msg
//            )
            GlobalScope.launch {
                MainApplication.db.handDAO().agregar(
                    Hand(modelo.draw.value?.cards?.get(0)?.code!!, modelo.draw.value?.cards?.get(0)?.image!!)
                )
            }


//            val lastCard = MainApplication.db.handDAO().getLastCardSync()
//            modelo.anterior.postValue(lastCard)

//            val taskLC = GetLastCardTask()
//            modelo.anterior.postValue(taskLC.execute().get())

//            lastCardHandler.handler?.sendMessage(Message())
//            GlobalScope.launch {
//                val lastCard = MainApplication.db.handDAO().getLastCardSync()
//                modelo.anterior.postValue(lastCard)
//            }
//            modelo.anterior.postValue(lastCardHandler.lastCard)

//            Thread.sleep(500)
//            lastCardHandler.interrupt()

        }


//        MainApplication.db.handDAO().getLastCard().observe(this){ modelo.anterior.postValue(it) }


        modelo.hand.observe(this){
            Log.d("hand",it.toString())
            Log.d("hand",it.piles[JUGADOR].toString())
            val info = Gson().fromJson(it.piles[JUGADOR].toString(),PileRem::class.java)
            Log.d("hand",info.toString())

        }

        modelo.username.observe(this){
            editSharedPrefs()?.apply {
                putString(SharedPrefAcces.USERNAME,it)
            }?.commit()
        }

        modelo.deck.observe(this){
            editSharedPrefs()?.apply {
                putString(SharedPrefAcces.DECK,it.toJson())
            }?.commit()
        }

        modelo.status.observe(this){
            Log.d("status",it.toString())
        }


        Log.e("corutine","onCreate")
        GlobalScope.launch {
            Log.d("corutine","Inicio Tarea Dummy")
            delay(10000)
            Log.d("corutine","Fin Tarea Dummy")
        }

//        runBlocking {
//            delay(1000000) //Causar un ANR
//        }
//        runBlocking {
//            val deref = async { getInt() }
//            val deref2 = async { getLong() }
//            Log.d("corutine","Iniciando Espera")
//
//            Log.d("corutine","Resultado 2 ${deref2.await()}")
//            Log.d("corutine","Resultado 1 ${deref.await()} ")
//            Log.d("corutine","Fin Espera Espera")
//        }

//        GlobalScope.launch {
//            val job = launch {
//                repeat(500){
//                    delay(100)
//                    Log.d("corutine","Resultado $it")
//                }
//            }
//            delay(5000)
//            Log.d("corutine","Te tardaste")
//            job.cancelAndJoin()
//            Log.d("corutine","No acabo")
//        }
//
//        runBlocking {
//            try {
//                withTimeout(5000){
//                    val job = launch {
//                        repeat(500){
//                            delay(100)
//
//                            Log.d("corutine","Resultado2 ${it}")
//                        }
//                    }
//                }
//            }catch (ex:TimeoutCancellationException){
//                Log.d("corutine","No acabo")
//            }
//
//
//        }

        Log.e("corutine","onCreate")

//        startService(
//            Intent(applicationContext, MyService::class.java)
//        )
        bindService(
            Intent(this, MyService::class.java), mServiceConnection,
            BIND_AUTO_CREATE
        )

    }

    override fun onStop() {
        unbindService(mServiceConnection)
        super.onStop()
    }

//    fun nuevoDeck(view: View){
//        nuevoDeck(modelo)
//    }

    override fun onStart() {
        super.onStart()
        JUGADOR = sharedPrefs()?.getString(SharedPrefAcces.USERNAME, "No_Name")!!
        modelo.username.postValue(JUGADOR)
        val deckT = Deck.fromJson( sharedPrefs()?.getString(SharedPrefAcces.DECK, "{}")!! )
        modelo.deck.postValue(deckT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("code",requestCode.toString())
        Log.d("result",resultCode.toString())
        Log.d("data",data.toString())
    }

    companion object{

//        const val JUGADOR = "Chema"

        @JvmStatic
        @BindingAdapter("cardImage")
        fun loadImage(view:ImageView , url:String?){

            url?.let{
                Glide.with(view.context).load(url).into(view)
            }
//Esta parte remplazo al observer y el vinculo esta en el xml
//        modelo.draw.observe(this){
//            Log.d("draw",it.toString())
//            Glide.with(this).load(
//                it.cards[0].image
//            )
////                .circleCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
//                .into(imageView)
//        }

        }
    }

    suspend fun getInt():Int{
        delay(1000)
        return Random(1).nextInt()
    }

    suspend fun getLong():Long{
        delay(2000)
        return Random(1).nextLong()
    }

}
