package mx.chemasmas.cursos.databinding2.concurrencia

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import mx.chemasmas.cursos.databinding2.MainApplication
import mx.chemasmas.cursos.databinding2.room.Hand
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel

class AddToHandLooper(val modelo: BaseViewModel) : Thread() {

    var handler:Handler? = null
    override fun run() {
        try{
            Looper.prepare();

            handler = object:Handler(Looper.myLooper()!!){
                override fun handleMessage(msg: Message) {
                    MainApplication.db.handDAO().agregar(
                            msg.obj as Hand
                    )
                }
            }

            Looper.loop()

            //El codigo que pongamos a continuacion solo se ejecuta despues del quit
        }catch (t:Throwable){
            Log.e("Looper", "Se murio")
        }
    }
}