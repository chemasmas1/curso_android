package mx.chemasmas.cursos.firebasedemo.interfaces

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import mx.chemasmas.cursos.firebasedemo.model.UserViewModel

class FacebookLoginImpl : FacebookLogin {
    private lateinit var activity: Activity
    private val TAG: String = "Facebook"
    private lateinit var modelo: UserViewModel
    private var callbackManager: CallbackManager? = null
    private lateinit var auth: FirebaseAuth
    private val defaultCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
//            result?.accessToken

            Log.d(TAG, "facebook:onSuccess:$result")

            handleFacebookAccessToken(result!!.accessToken)

        }

        override fun onCancel() {
            Log.d(TAG, "facebook:onCancel")

        }

        override fun onError(error: FacebookException?) {
            Log.d(TAG, "facebook:onError", error)

        }

    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
//                    model.uid.postValue(id)
                    modelo.gUser.postValue(auth.currentUser)
                } else {
                    //                    model.gUser.postValue(null)
                    modelo.fatal.postValue(
                        Throwable("Fallo Con el usuario de Google")
                    )
                }
            }
    }

    override fun initFLogin() {

    }

    override fun create(model: UserViewModel, loginButton: LoginButton, act: Activity) {
        modelo = model
        auth = Firebase.auth
        activity = act
        callbackManager = CallbackManager.Factory.create();

        loginButton.setPermissions("email")
        loginButton.registerCallback(callbackManager,defaultCallback)
    }

    override fun activityFaceResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }
}