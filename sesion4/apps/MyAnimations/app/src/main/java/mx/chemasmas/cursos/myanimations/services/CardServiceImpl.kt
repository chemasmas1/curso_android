package mx.chemasmas.cursos.myanimations.services

import android.util.Log
import mx.chemasmas.cursos.myanimations.models.Deck
import mx.chemasmas.cursos.myanimations.models.Draw
import mx.chemasmas.cursos.myanimations.models.GameStatus
import mx.chemasmas.cursos.myanimations.models.PileInfo
import mx.chemasmas.cursos.myanimations.viewmodel.BaseViewModel
import mx.chemasmas.cursos.myanimations.MainApplication
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardServiceImpl : CardService {

    val cliente = MainApplication.cliente

    override fun nuevoDeck(modelo: BaseViewModel) {
        cliente?.getNewDeck()?.enqueue(DeckGen(modelo))
    }

    override fun drawOneCard(id_deck:String,modelo: BaseViewModel?) {
        cliente?.drawNCards(id_deck)?.enqueue(DrawCard(modelo))
    }

    override fun drawOneCard(id_deck: String): Draw? {
        return cliente?.drawNCards(id_deck)?.execute()?.body()
    }

    override fun addToHand(id_deck:String,pileName:String,card:String,modelo: BaseViewModel) {
        cliente?.addToPile(id_deck,pileName,card)?.enqueue(AddToHand(modelo))
    }

    override fun getStatus(id_deck: String, pileName: String,modelo: BaseViewModel) {
        cliente?.gameStatus(id_deck,pileName)?.enqueue(GetStatus(modelo))
    }

    inner class DeckGen(val modelo: BaseViewModel) : Callback<Deck> {
        override fun onResponse(call: Call<Deck>, response: Response<Deck>) {
            if(response.isSuccessful){
                Log.d("DeckService", response.body().toString())
                modelo.deck.postValue(response.body())
            }
        }

        override fun onFailure(call: Call<Deck>, t: Throwable) {

        }

    }

    inner class DrawCardNotif() : Callback<Draw> {
        var draw: Draw? = null
        override fun onResponse(call: Call<Draw>, response: Response<Draw>) {
            if(response.isSuccessful){
                draw = response.body()
            }
        }

        override fun onFailure(call: Call<Draw>, t: Throwable) {
        }

    }

    inner class DrawCard(val modelo: BaseViewModel?) : Callback<Draw> {
        override fun onResponse(call: Call<Draw>, response: Response<Draw>) {
            if(response.isSuccessful){
                modelo?.let {
                    it.draw.postValue(response.body())
                } ?: {
                    Log.d("servcie",response.body().toString())
                }()
            }
        }

        override fun onFailure(call: Call<Draw>, t: Throwable) {
        }

    }
    inner class AddToHand(val modelo: BaseViewModel) : Callback<PileInfo> {
        override fun onResponse(call: Call<PileInfo>, response: Response<PileInfo>) {
            if(response.isSuccessful){
                modelo.hand.postValue(response.body())
            }
        }

        override fun onFailure(call: Call<PileInfo>, t: Throwable) {

        }
    }

    inner class GetStatus(val modelo: BaseViewModel) : Callback<GameStatus> {
        override fun onResponse(call: Call<GameStatus>, response: Response<GameStatus>) {
            if(response.isSuccessful){
                modelo.status.postValue(response.body())
            }
        }

        override fun onFailure(call: Call<GameStatus>, t: Throwable) {
        }

    }
}