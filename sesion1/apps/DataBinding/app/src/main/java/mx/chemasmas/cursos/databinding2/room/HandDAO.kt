package mx.chemasmas.cursos.databinding2.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HandDAO {

    @Insert
    fun agregar( hand: Hand )

    @Query("Select * from manos")
    fun getHands(): List<Hand>

    @Query("Select * from manos")
    fun getRealTimehands():LiveData<List<Hand>>

    @Query("Select * from manos ORDER BY id DESC LIMIT 1")
    fun getLastCard():LiveData<Hand>

    @Query("Select * from manos ORDER BY id DESC LIMIT 1")
    fun getLastCardSync():Hand
}