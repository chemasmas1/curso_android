package mx.chemasmas.cursos.firebasedemo.model

data class User(val uid:String,val email: String?, val providerId: String, val displayName: String?)