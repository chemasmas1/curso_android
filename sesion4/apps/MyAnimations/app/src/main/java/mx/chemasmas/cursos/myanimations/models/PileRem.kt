package mx.chemasmas.cursos.myanimations.models

import java.io.Serializable
import androidx.annotation.Keep

@Keep
data class PileRem(
    val remaining: Int // 2
) : Serializable