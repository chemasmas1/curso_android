package mx.chemasmas.cursos.databinding2.concurrencia

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import mx.chemasmas.cursos.databinding2.MainApplication
import mx.chemasmas.cursos.databinding2.room.Hand
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel

class GetLastCardLooper(val modelo: BaseViewModel) :Thread() {

    var handler: Handler? = null
    var lastCard: Hand? = null
    override fun run() {
        Looper.prepare()
        Log.d("loop","repite")


        handler = object :Handler(Looper.myLooper()!!){
//        handler = object :Handler(Looper.getMainLooper()){
            override fun handleMessage(msg: Message) {
                Log.d("loop","inicio")
                Thread.sleep(10000)
                lastCard = MainApplication.db.handDAO().getLastCardSync()
                modelo.anterior.postValue(lastCard)
                Log.d("loop","end")
            }
        }
        Looper.loop()
    }
}