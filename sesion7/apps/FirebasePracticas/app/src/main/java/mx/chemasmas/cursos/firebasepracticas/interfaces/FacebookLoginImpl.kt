package mx.chemasmas.cursos.firebasepracticas.interfaces

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import mx.chemasmas.cursos.firebasepracticas.viewmodel.UserViewModel

class FacebookLoginImpl : FacebookLogin {

    companion object{
        const val TAG = "Login Facebook"
    }
    private lateinit var activity: Activity
    private lateinit var modelo: UserViewModel
    private lateinit var auth: FirebaseAuth
    private lateinit var callbackManager: CallbackManager
    private val defaultCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
//            result?.accessToken

            Log.d(TAG, "facebook:onSuccess:$result")

            firebaseAuthWithFacebook(result!!.accessToken)

        }

        override fun onCancel() {
            Log.d(TAG, "facebook:onCancel")

        }

        override fun onError(error: FacebookException?) {
            Log.d(TAG, "facebook:onError", error)

        }

    }

    override fun create(model: UserViewModel, loginButton: LoginButton, act: Activity) {
        modelo = model
        auth = Firebase.auth
        activity = act
        callbackManager = CallbackManager.Factory.create()

        loginButton.setPermissions("email")
        loginButton.registerCallback(callbackManager,defaultCallback)
    }

//    override fun initFLogin() {
//
//    }

    override fun firebaseAuthWithFacebook(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
//                    model.uid.postValue(id)
                    modelo.gUser.postValue(auth.currentUser)
                } else {
                    //                    model.gUser.postValue(null)
                    modelo.fatal.postValue(
                        Throwable("Fallo Con el usuario de Google")
                    )
                }

                // ...
            }
    }

    override fun activityFacebookResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }
}