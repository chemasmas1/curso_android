package mx.chemasmas.cursos.catalogolayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun goToLayout(v: View){
        val clase = when(v.id){
            R.id.linear-> LinearActivity::class.java
            R.id.table-> TabActivity::class.java
            R.id.frame-> FrameLayout::class.java
            R.id.constraint-> ConstraintLayout::class.java
            R.id.view_catalog-> CatalogoViews::class.java
            else -> null
        }
        clase?.let {
            val i = Intent(this,it)
            startActivity(i)
        }
    }
}