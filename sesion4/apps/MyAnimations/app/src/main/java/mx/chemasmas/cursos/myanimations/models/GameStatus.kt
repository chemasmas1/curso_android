package mx.chemasmas.cursos.myanimations.models

import com.google.gson.JsonObject


//{
//    "success": true,
//    "deck_id": "d5x0uw65g416",
//    "remaining": "42",
//    "piles": {
//    "player1": {
//    "remaining": "3"
//},
//    "player2": {
//    "cards": [
//    {
//        "image": "https://deckofcardsapi.com/static/img/KH.png",
//        "value": "KING",
//        "suit": "HEARTS",
//        "code": "KH"
//    },
//    {
//        "image": "https://deckofcardsapi.com/static/img/8C.png",
//        "value": "8",
//        "suit": "CLUBS",
//        "code": "8C"
//    }
//    ],
//    "remaining": "2"
//}
//},
//}
data class GameStatus(
    val success:Boolean,
    val deck_id:String,
    val remaining:String,
    val piles: JsonObject
)
