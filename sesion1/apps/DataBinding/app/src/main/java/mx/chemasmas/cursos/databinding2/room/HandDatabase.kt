package mx.chemasmas.cursos.databinding2.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database( entities = [Hand::class] , version = 3)
abstract class HandDatabase :RoomDatabase(){
    abstract fun handDAO():HandDAO
}