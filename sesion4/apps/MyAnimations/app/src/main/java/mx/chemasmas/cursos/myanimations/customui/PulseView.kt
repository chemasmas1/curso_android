package mx.chemasmas.cursos.myanimations.customui

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator


class PulseView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    private var mY:Float  = 0f
    private var mX: Float = 0f
    private var mRadius: Float = 0f
    private val mPaint: Paint = Paint()
    private val COLOR_ADJUSTER = 5
    private val ANIMATION_DURATION = 4000L
    private val ANIMATION_DELAY: Long = 1000
    private val mPulseAnimatorSet = AnimatorSet()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(mX, mY, mRadius, mPaint)
    }


    fun setRadius(radius: Float) {
        mRadius = radius
        mPaint.color = Color.GREEN + radius.toInt() / COLOR_ADJUSTER
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.actionMasked == MotionEvent.ACTION_DOWN) {
            mX = event.x
            mY = event.y
            if(mPulseAnimatorSet != null && mPulseAnimatorSet.isRunning()) {
                mPulseAnimatorSet.cancel();
            }
            mPulseAnimatorSet.start();
        }

        return super.onTouchEvent(event)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        val growAnimator = ObjectAnimator.ofFloat(
            this,
            "radius", 0f, width.toFloat()
        )
        growAnimator.duration = ANIMATION_DURATION
        growAnimator.interpolator = LinearInterpolator()
        val shrinkAnimator = ObjectAnimator.ofFloat(
            this,
            "radius", width.toFloat(), 0f
        )
        shrinkAnimator.duration = ANIMATION_DURATION
        shrinkAnimator.interpolator = LinearOutSlowInInterpolator()
        shrinkAnimator.startDelay = ANIMATION_DELAY;

        val repeatAnimator = ObjectAnimator.ofFloat(
            this,
            "radius", 0f, width.toFloat()
        )
        repeatAnimator.startDelay = ANIMATION_DELAY
        repeatAnimator.duration = ANIMATION_DURATION

        repeatAnimator.repeatCount = 1;
        repeatAnimator.repeatMode = ValueAnimator.REVERSE;

        mPulseAnimatorSet.play(growAnimator).before(shrinkAnimator);
        mPulseAnimatorSet.play(repeatAnimator).after(shrinkAnimator);

    }
}