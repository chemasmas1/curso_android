package mx.chemasmas.cursos.databinding2.models

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/*
{
    "success": true,
    "deck_id": "3p40paa87x90",
    "shuffled": true,
    "remaining": 52
}
 */
data class Deck(
    val success: Boolean,
    var deck_id: String,
//    val shuffled: Boolean,
    @SerializedName("shuffled")
    val mezclado:Boolean,
    val remaining: Int
):Serializable{

    fun toJson():String{
        return Gson().toJson(this)
    }

    companion object{
        fun fromJson(jsonString:String):Deck{
            return Gson().fromJson(jsonString,Deck::class.java)
        }
    }



}