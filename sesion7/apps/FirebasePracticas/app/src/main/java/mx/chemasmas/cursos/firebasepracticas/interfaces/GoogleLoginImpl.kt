package mx.chemasmas.cursos.firebasepracticas.interfaces

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import mx.chemasmas.cursos.firebasepracticas.viewmodel.UserViewModel
import java.util.logging.Logger

class GoogleLoginImpl : GoogleLogin {
    companion object{
        const val GOOGLE_LOGIN = 9999
    }

    private lateinit var activity: Activity
    private lateinit var gso: GoogleSignInOptions
    private lateinit var model: UserViewModel
    private lateinit var auth: FirebaseAuth

    override fun create(modelo: UserViewModel, defaultWebClient: String, act: Activity) {
        model = modelo
        auth = Firebase.auth
        activity = act
        gso = GoogleSignInOptions.Builder(
            GoogleSignInOptions.DEFAULT_SIGN_IN
        )
        .requestIdToken(defaultWebClient)
        .requestEmail()
        .requestProfile()
        .build()
    }

    override fun initGLogin() {
        activity.startActivityForResult(
            GoogleSignIn.getClient(activity,gso).signInIntent, GOOGLE_LOGIN
        )
    }

    override fun firebaseAuthWithGoogle(idToken: String, id: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(activity) { task ->
            if (task.isSuccessful) {
//                    model.uid.postValue(id)
                model.gUser.postValue(auth.currentUser)
            } else {
                //                    model.gUser.postValue(null)
                model.fatal.postValue(
                    Throwable("Fallo Con el usuario de Google")
                )
            }

            // ...
        }
    }

    override fun activityGoogleResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode != GOOGLE_LOGIN ) return
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            val account = task.getResult(ApiException::class.java)!!
            firebaseAuthWithGoogle(account.idToken!!,account.id!!)
        }catch (e:ApiException){
            model.fatal.postValue(
                e.cause
            )
        }
    }
}