package mx.chemasmas.cursos.databinding2.concurrencia

import android.os.AsyncTask
import mx.chemasmas.cursos.databinding2.MainApplication
import mx.chemasmas.cursos.databinding2.room.Hand

class AddToHandTask: AsyncTask<Hand, Unit, Unit>() {

    override fun doInBackground(vararg params: Hand?) {

        MainApplication.db.handDAO().agregar(
            params[0]!!
        )
    }
}