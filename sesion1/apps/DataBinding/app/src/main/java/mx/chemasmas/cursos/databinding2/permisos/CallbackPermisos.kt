package mx.chemasmas.cursos.databinding2.permisos

interface CallbackPermisos {

    fun onPermisosExitosos( permisos: List<String> )
    fun onPermisosFallo( permisos: List<String> )
}