package mx.chemasmas.cursos.databinding2.util

import android.content.Context
import android.content.SharedPreferences

object SharedPrefAcces {
    const val spFile = "datos"
    const val USERNAME:String = "username"
    const val DECK:String = "deck"
}

fun Context.sharedPrefs():SharedPreferences?{
    return this.getSharedPreferences(SharedPrefAcces.spFile,Context.MODE_PRIVATE)
}

fun Context.editSharedPrefs():SharedPreferences.Editor? {
    return sharedPrefs()?.edit()
}