package mx.chemasmas.cursos.firebasepracticas.interfaces

import android.app.Activity
import android.content.Intent
import com.facebook.AccessToken
import com.facebook.login.widget.LoginButton
import mx.chemasmas.cursos.firebasepracticas.viewmodel.UserViewModel

interface FacebookLogin {
    fun create(model: UserViewModel, loginButton: LoginButton, act: Activity)
//    fun initFLogin()
    fun firebaseAuthWithFacebook(idToken: AccessToken)
    fun activityFacebookResult(requestCode: Int, resultCode: Int, data: Intent?)
}