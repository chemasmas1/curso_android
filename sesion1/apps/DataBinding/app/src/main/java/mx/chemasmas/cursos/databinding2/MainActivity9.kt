package mx.chemasmas.cursos.databinding2

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPhotoResponse
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import mx.chemasmas.cursos.databinding2.BuildConfig.MAPS_API_KEY
import mx.chemasmas.cursos.databinding2.permisos.CallbackPermisos
import mx.chemasmas.cursos.databinding2.permisos.PermisosActivity

class MainActivity9 : PermisosActivity(), OnMapReadyCallback {


    private var imageView7: ImageView? = null
    private lateinit var myMap: GoogleMap
    val TAG = "places"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main9)

        imageView7 = findViewById<ImageView>(R.id.imageView7)

        requesPermisos(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            callbackPermisos = object : CallbackPermisos {
                override fun onPermisosExitosos(permisos: List<String>) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                        requesPermisos(
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        )
                    }
                }

                override fun onPermisosFallo(permisos: List<String>) {
                    Log.e(tag, "Permission denied [ $permisos ]")
                    //
                }

            }
        )

        val mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.map,mapFragment)
            .commit()

        mapFragment.getMapAsync(this)
        Places.initialize(applicationContext, MAPS_API_KEY)

        funcion1()

    }

    private fun funcion1() {
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME,
            Place.Field.OPENING_HOURS,
            Place.Field.LAT_LNG,
            Place.Field.PHOTO_METADATAS
        ))

        val  placesClient = Places.createClient(this)

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: ${place.name}, ${place.id} , ${place.openingHours}")
                myMap.moveCamera(
                    CameraUpdateFactory.newLatLng(
                        place.latLng
                    )
                )
                myMap.addMarker(
                    MarkerOptions()
                        .title(place.name)
                        .position(place.latLng!!)
                        .snippet(
                            place.openingHours.toString() ?: ""
                        )
                        .infoWindowAnchor(0f,0f)
                )



                val fields = listOf(Place.Field.PHOTO_METADATAS)
                val placeRequest = FetchPlaceRequest.newInstance(place.id!!, fields)
                placesClient.fetchPlace(placeRequest).addOnSuccessListener {
                    val metada = place.photoMetadatas
                    if (metada == null || metada.isEmpty()) {
                        Log.w(TAG, "No photo metadata.")
                        return@addOnSuccessListener
                    }
                    val photoMetadata = metada.first()

                    // Get the attribution text.
                    val attributions = photoMetadata?.attributions

                    // Create a FetchPhotoRequest.
                    val photoRequest = FetchPhotoRequest.builder(photoMetadata)
                        .setMaxWidth(500) // Optional.
                        .setMaxHeight(300) // Optional.
                        .build()
                    placesClient.fetchPhoto(photoRequest)
                        .addOnSuccessListener { fetchPhotoResponse: FetchPhotoResponse ->
                            val bitmap = fetchPhotoResponse.bitmap
                            imageView7?.setImageBitmap(bitmap)
                        }.addOnFailureListener { exception: Exception ->
                            if (exception is ApiException) {
                                Log.e(TAG, "Place not found: " + exception.message)
                                val statusCode = exception.statusCode
                                TODO("Handle error with given status code.")
                            }
                        }
                }


            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: $status")
            }
        })
    }

    override fun onMapReady(mapa: GoogleMap?) {

        mapa?.moveCamera(
            CameraUpdateFactory.zoomTo(15f)
        )
        myMap = mapa!!
    }
}