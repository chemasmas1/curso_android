package mx.chemasmas.cursos.myanimations.customui

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.addTextChangedListener


class EditTextClearable @JvmOverloads constructor(context: Context, attrs: AttributeSet, defStyleAttr:Int = com.google.android.material.R.attr.editTextStyle): AppCompatEditText(context,  attrs, defStyleAttr){
//class EditTextClearable @JvmOverloads constructor(context: Context, attrs: AttributeSet?=null, defStyleAttr:Int = 0 ): EditText(context,  attrs, defStyleAttr){  // Usaremos los componentes de androidx
//class EditTextClearable : AppCompatEditText{

//    constructor(context: Context):super(context)
//    constructor(context: Context,attrs: AttributeSet):super(context,attrs)
//    constructor(context: Context,attrs: AttributeSet,defStyleAttr: Int):super(context,attrs,defStyleAttr)

    private var clearButtonImage: Drawable = ResourcesCompat.getDrawable(resources,android.R.drawable.ic_menu_close_clear_cancel,null)!!

    init {

//        setCompoundDrawablesRelativeWithIntrinsicBounds(
//                null,null,clearButtonImage,null
//        )
        setOnTouchListener { v, event ->

            var clicked = false
            val drawableDer: Drawable? = compoundDrawablesRelative[2]
            if (drawableDer != null) {
                val clearStart = width - paddingEnd - clearButtonImage?.intrinsicWidth
                if(event.x > clearStart){
                    clicked = true
                }
            }
            if(clicked){
                when(event.action){
                    MotionEvent.ACTION_DOWN -> showClear()
                    MotionEvent.ACTION_UP -> {
                        text?.clear()
                        hideClear()
                    }
                }
            }
            false
        }

        addTextChangedListener(
            { _, _, _, _ ->
                //No nos interesa
            },{ _, _, _, _ ->
                showClear()
            },{
                //Tampoco
            }
        )
    }

    fun showClear(){
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,null,clearButtonImage,null
        )
    }

    fun hideClear(){
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,null,null,null
        )
    }

}

