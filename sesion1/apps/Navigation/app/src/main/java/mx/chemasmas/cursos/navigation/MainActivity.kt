package mx.chemasmas.cursos.navigation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun goToLayout(v: View){
        val clase = when(v.id){
            R.id.drawer-> Drawer::class.java
            R.id.bottom-> BottomNav::class.java
            R.id.appbar-> ToolbarActivity::class.java
            else -> null
        }
        clase?.let {
            val i = Intent(this,it)
            startActivity(i)
        }
    }
}