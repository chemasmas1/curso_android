package mx.chemasmas.cursos.firebasedemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.BaseAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import mx.chemasmas.cursos.firebasedemo.adapters.MensajeAdapter
import mx.chemasmas.cursos.firebasedemo.databinding.ActivityMain2Binding
import mx.chemasmas.cursos.firebasedemo.model.Mensaje
import mx.chemasmas.cursos.firebasedemo.model.User

class MainActivity2 : AppCompatActivity() {
    private lateinit var adapter: MensajeAdapter
    private lateinit var bindig: ActivityMain2Binding
    private val TAG = "RTDB"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main2)
        bindig =  DataBindingUtil.setContentView(this,R.layout.activity_main2)

        adapter = MensajeAdapter()
        bindig.chat.adapter = adapter

        /* Paso 1*/
        val database = Firebase.database
        val myRef = database.getReference("message")
        myRef.setValue("Hello, Test!")

        // Paso 2
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.value
                Log.d(TAG, "Value is: $value")
                Toast.makeText(this@MainActivity2,value.toString(),Toast.LENGTH_SHORT).show()
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })

        /*Paso3*/
        val userRef = database.getReference("users")
        val mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth.currentUser
//
        currentUser?.let{
            userRef.child(it.uid).setValue(
                User(it.uid,it.email,it.providerId,it.displayName)
            )
//            userRef.setValue( User(it.uid,it.email,it.providerId,it.displayName) )
        }

        /*Paso 4*/
        val chatsRef = database.getReference("chats")
//        val idChat = "${System.currentTimeMillis()}-${currentUser!!.uid}"
//        chatsRef.child(
//            idChat
//        ).setValue(
//            Mensaje(idChat,currentUser.email,"Esto es un mensaje de chat")
//        )

        chatsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.value
                Log.d(TAG, "Value is: $value")
                val parsed  = value as HashMap<String,HashMap<String,String>>
                Log.d(TAG, "Value is: $parsed")
                adapter.showMessages(parsed)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })
        chatsRef.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val value = snapshot.value
                Log.d(TAG, "Value is: $value")
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })

        /*5*/
        bindig.enviar.setOnClickListener {
            val idChat = "${System.currentTimeMillis()}-${currentUser!!.uid}"
            chatsRef.child(
                idChat
            ).setValue(
                Mensaje(
                    idChat,
                    currentUser.displayName?.split(" ")?.first() ?: "No Name",
                    currentUser.email,
                    bindig.mensaje.text.toString()
                )
            )
        }


    }
}

