package mx.chemasmas.cursos.myanimations.room

import androidx.room.Database
import androidx.room.RoomDatabase
import mx.chemasmas.cursos.myanimations.room.Hand
import mx.chemasmas.cursos.myanimations.room.HandDAO

@Database( entities = [Hand::class] , version = 3)
abstract class HandDatabase :RoomDatabase(){
    abstract fun handDAO(): HandDAO
}