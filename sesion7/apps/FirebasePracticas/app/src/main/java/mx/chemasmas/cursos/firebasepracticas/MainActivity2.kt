package mx.chemasmas.cursos.firebasepracticas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import mx.chemasmas.cursos.firebasepracticas.adapters.MensajesAdapter
import mx.chemasmas.cursos.firebasepracticas.databinding.ActivityMain2Binding
import java.io.IOException

class MainActivity2 : AppCompatActivity() {

    companion object{
        const val TAG = "RTDB"
    }

    private lateinit var binding: ActivityMain2Binding
    val database = Firebase.database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_main2)
//        val ref = database.getReference("Chemasmas")
//        ref.setValue("Hola Mundo!!!")

//        ref.addValueEventListener(object :ValueEventListener{
//            override fun onDataChange(snapshot: DataSnapshot) {
//                val value = snapshot.value
//                Log.d(TAG, "Value is: $value")
//                Toast.makeText(this@MainActivity2,value.toString(), Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onCancelled(error: DatabaseError) {
//                Log.w(TAG, "Failed to read value.", error.toException())
//            }
//
//        })

        val chatsRef = database.getReference("chats")
        val mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth.currentUser
        val adapter = MensajesAdapter()
        binding.chat.adapter = adapter

        binding.enviar.setOnClickListener {
            val idChat = "${System.currentTimeMillis()}-${currentUser!!.uid}"
//            throw IOException("Eso no es valido!!!!")
            chatsRef.child(idChat).setValue(
//                binding.mensaje.text.toString()
                 Mensaje(idChat,currentUser.displayName,currentUser.email,binding.mensaje.text.toString())
            )
            binding.mensaje.setText("")
        }

        chatsRef.addChildEventListener(
            object : ChildEventListener {
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    val value = snapshot.value
                    Log.d(TAG, "Value is: $value")
                    adapter.addMsj( value as HashMap<String, String>)
                    adapter.notifyDataSetChanged()
                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                }

                override fun onChildRemoved(snapshot: DataSnapshot) {

                }

                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

                }

                override fun onCancelled(error: DatabaseError) {

                }

            }
        )



        val allListen = object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val value = snapshot.value
                Log.d(TAG, "Value is: $value")
                val parsed  = value as HashMap<String,HashMap<String,String>>
                //PRocesarlos
                adapter.showMessages(parsed )
                adapter.notifyDataSetChanged()
                chatsRef.removeEventListener(this)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }

        }
        chatsRef.addValueEventListener(allListen)


        Firebase.messaging.token.addOnSuccessListener {
            Log.d("Notif",it)
        }

        Firebase.messaging.subscribeToTopic(
            "curso"
        ).addOnSuccessListener {
            Log.d("Notif","Suscrito")
        }





    }
}