package mx.chemasmas.cursos.firebasedemo

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import mx.chemasmas.cursos.firebasedemo.databinding.ActivityMainBinding
import mx.chemasmas.cursos.firebasedemo.interfaces.FacebookLogin
import mx.chemasmas.cursos.firebasedemo.interfaces.FacebookLoginImpl
import mx.chemasmas.cursos.firebasedemo.interfaces.GoogleLogin
import mx.chemasmas.cursos.firebasedemo.interfaces.GoogleLoginImpl
import mx.chemasmas.cursos.firebasedemo.model.UserViewModel
import java.security.MessageDigest


class MainActivity : AppCompatActivity(),
    GoogleLogin by GoogleLoginImpl(),
    FacebookLogin by FacebookLoginImpl()
{

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var binding: ActivityMainBinding
    private var mAuth: FirebaseAuth? = null
    val TAG = "LOGIN"
    val model: UserViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//        setContentView(R.layout.activity_main)


        create(model, getString(R.string.default_web_client_id), this)
        binding.signInButton.setOnClickListener {
            initGLogin()
        }

        binding.logout.setOnClickListener {
            closeSesion()
        }

        observers()
        mAuth = FirebaseAuth.getInstance()
        updateUI()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            hashThis()
        }

//        FacebookSdk.sdkInitialize(applicationContext);
//        AppEventsLogger.activateApp(this);


        create(model,binding.loginButton,this)

    }


    private fun updateUI() {
        val currentUser = mAuth!!.currentUser
        currentUser?.email?.let { Log.d(TAG, it) }
        currentUser?.providerId?.let { Log.d(TAG, it) }

        currentUser?.let {
            Intent(this,MainActivity2::class.java).apply {
                startActivity(this)
            }
        }
    }

    private fun observers() {
        model.gUser.observe(this){
            it ?: return@observe
            updateUI()

//            Snackbar.make(binding.root, "Accediste!!", Snackbar.LENGTH_LONG).show()
            //Hacer Login RedSocial
//            login(
//                    it.email,null,model.uid.value,model
//            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activityFaceResult(requestCode, resultCode, data)
        activityResutlt(requestCode, resultCode, data)
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun hashThis() {

        val info = packageManager.getPackageInfo(
            "mx.chemasmas.cursos.firebasedemo",
            PackageManager.GET_SIGNING_CERTIFICATES
        )


        for (signature in info.signingInfo.apkContentsSigners) {
            //for (signature in info.signatures) {
            val md: MessageDigest = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
        }

    }
}