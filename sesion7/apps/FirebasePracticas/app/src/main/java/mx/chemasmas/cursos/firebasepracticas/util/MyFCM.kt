package mx.chemasmas.cursos.firebasepracticas.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.firebasepracticas.MainActivity
import mx.chemasmas.cursos.firebasepracticas.R
import java.net.URL

class MyFCM: FirebaseMessagingService() {

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        val refreshedToken = FirebaseMessaging.getInstance().token
        Log.d("Notif", "Refreshed token: $refreshedToken")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("Notif", "$remoteMessage")

        val notif = remoteMessage.notification
        val title = remoteMessage.data["title"] ?: notif?.title ?: "No tenemos titulo"
        val mesage = remoteMessage.data["body"] ?: notif?.body ?: "No tenemos Body"

        Log.d("Notif", "$title - $mesage")

        processExtras(remoteMessage.data)


        showNotifications(title,mesage,notif?.imageUrl)
    }

    private fun processExtras(data: MutableMap<String, String>) {
        GlobalScope.launch {
            data.forEach { (t, u) ->
                Log.d("Notif Extra", "$t -> $u")
            }
        }
    }

    private fun showNotifications(title: String, mesage: String, imageUrl: Uri?) {
        val i = Intent(this,MainActivity::class.java)
        ///

        val pendingIntent = PendingIntent.getActivity(this,1,i,
            PendingIntent.FLAG_ONE_SHOT)

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = getString(R.string.default_notification_channel_id)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(channelId,"my_channel",NotificationManager.IMPORTANCE_HIGH)
            mChannel.description = "Notificaciones Push desde Firebase"
            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mChannel.enableLights(true)
            manager.createNotificationChannel(mChannel)
        }

        val myBitmap =
            imageUrl?.let {
                BitmapFactory.decodeStream( URL(it.toString()).openStream() )
            } ?: BitmapFactory.decodeResource(resources,R.drawable.com_facebook_favicon_blue)


        val notification: Notification = NotificationCompat.Builder(this,channelId)
            .setContentText(mesage)
            .setContentTitle(title)
//            .setContentIntent(pendingIntent)
//            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setSmallIcon(R.drawable.com_facebook_favicon_blue)
            .setLargeIcon(myBitmap)
            .setStyle(
                NotificationCompat.BigPictureStyle()
                .bigPicture(myBitmap)
                .bigLargeIcon(null)
            )
//            .setSmallIcon(R.drawable.logo_roadmasters )
            //.setLargeIcon( ContextCompat.getDrawable(this,R.drawable.logo_ggi)!!.toBitmap() )
//            .setExtras(intent.extras)
//            .setStyle(
//                NotificationCompat.BigTextStyle()
//            )
            .setAutoCancel(true)
            .build()


        manager.notify(6578, notification)

    }
}