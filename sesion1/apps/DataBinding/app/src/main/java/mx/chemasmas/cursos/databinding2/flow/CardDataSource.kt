package mx.chemasmas.cursos.databinding2.flow

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.isActive
import mx.chemasmas.cursos.databinding2.MainApplication
import mx.chemasmas.cursos.databinding2.models.Card
import mx.chemasmas.cursos.databinding2.models.Draw
import mx.chemasmas.cursos.databinding2.services.CardServiceImpl
import mx.chemasmas.cursos.databinding2.services.CardsAPI
import java.util.concurrent.Flow

class CardDataSource(val idDeck:String)
{

    val cliente = MainApplication.cliente


    val tenCards = flow<ArrayList<Card>> {
        var handCounter = 0
        while(true){
            if(handCounter++ < 5){
                val draw = cliente?.drawNCards(idDeck,2)?.execute()?.body()
                emit(draw?.cards ?: arrayListOf())
                delay( 2000 )
            }
        }
    }

    val  twoCard = flow<ArrayList<Card>> {
        val draw = cliente?.drawNCards(idDeck,2)?.execute()?.body()
        emit(draw?.cards ?: arrayListOf())
    }
}

interface CardAPIsus {
    suspend fun getTenCards(): List<Card>
    suspend fun get2Cards():Array<Card>
}