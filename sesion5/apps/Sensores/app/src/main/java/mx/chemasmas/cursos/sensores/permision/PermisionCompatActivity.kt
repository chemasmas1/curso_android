package mx.chemasmas.cursos.sensores.permision

import android.content.pm.PackageManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

open class PermisionCompatActivity:AppCompatActivity() {

    private val tag = "Permissions extension"
    private val latestPermissionRequest = AtomicInteger()
    private val permissionRequests = ConcurrentHashMap<Int,
            List<String>>()
    private val permissionCallbacks =
        ConcurrentHashMap<List<String>, PermisosRequestCallback>()

    private val defaultPermissionCallback = object :
        PermisosRequestCallback {
        override fun onPermisionGranted(permisos: List<String>) {
            Log.i(tag, "Permission granted [ $permisos ]")
        }

        override fun onPermisionDenied(permisos: List<String>) {
            Log.e(tag, "Permission denied [ $permisos ]")
        }
    }

    fun requestPermissions(
        vararg permissions: String,
        callback: PermisosRequestCallback = defaultPermissionCallback
    ) {
        val id = latestPermissionRequest.incrementAndGet()
        val items = mutableListOf<String>()
        items.addAll(permissions)
        permissionRequests[id] = items
        permissionCallbacks[items] = callback
        ActivityCompat.requestPermissions(this, permissions, id)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        val items = permissionRequests[requestCode]
        items?.let {
            val callback = permissionCallbacks[items]
            callback?.let {
                var success = true
                for (x in 0..grantResults.lastIndex) {
                    val result = grantResults[x]
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        success = false
                        break
                    }
                }
                if (success) {
                    callback.onPermisionGranted(items)
                } else {
                    callback.onPermisionDenied(items)
                }
            }
        }
    }
}