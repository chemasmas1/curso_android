package mx.chemasmas.cursos.firebasedemo.interfaces

import android.app.Activity
import android.content.Intent
import com.facebook.login.widget.LoginButton
import mx.chemasmas.cursos.firebasedemo.MainActivity
import mx.chemasmas.cursos.firebasedemo.model.UserViewModel

interface FacebookLogin {


    fun initFLogin()
    fun create(model: UserViewModel, loginButton: LoginButton, mainActivity: Activity)
//    fun start()
//    fun firebaseAuthWithGoogle(idToken: String, id: String)
    fun activityFaceResult(requestCode: Int, resultCode: Int, data: Intent?)
//    fun closeSesion()
}