package mx.chemasmas.cursos.myanimations.customui


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import mx.chemasmas.cursos.myanimations.R


@SuppressLint("ResourceAsColor")
class FanControl @JvmOverloads constructor(
    context: Context, val attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

//    constructor(context: Context):super(context)
//    constructor(context: Context,attrs: AttributeSet):super(context, attrs)
//    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(
//        context,
//        attrs,
//        defStyleAttr
//    )

    private var mFanOffColor: Int = android.R.color.black
    private var mFanOnColor: Int = android.R.color.holo_green_dark
    private var SELECTION_COUNT = 4 // Total number of selections.

    private var mWidth // Custom view width.
            = 0f
    private var mHeight // Custom view height.
            = 0f
    private var mTextPaint // For text in the view.
            : Paint? = null
    private var mDialPaint // For dial circle in the view.
            : Paint? = null
    private var mRadius // Radius of the circle.
            = 0f
    private var mActiveSelection // The active selection.
            = 0

    // String buffer for dial labels and float for ComputeXY result.
    private val mTempLabel = StringBuffer(8)
    private val mTempResult = FloatArray(2)


    init {

        mTextPaint = Paint(Paint.ANTI_ALIAS_FLAG);
        with(mTextPaint!!){
            color = Color.BLACK;
            style = Paint.Style.FILL_AND_STROKE;
            textAlign = Paint.Align.CENTER;
            textSize = 40f;
        }


        mDialPaint = Paint(Paint.ANTI_ALIAS_FLAG);
        mDialPaint?.color = Color.GRAY;
        // Initialize current selection.
        mActiveSelection = 0;


        setOnClickListener {
            mActiveSelection = (mActiveSelection + 1) % SELECTION_COUNT;
            // Set dial background color to green if selection is >= 1.
            if (mActiveSelection >= 1) {
                mDialPaint?.color = mFanOnColor;
//                mDialPaint?.color = resources.getColor(mFanOnColor,null)
            } else {
                mDialPaint?.color = mFanOffColor;
//                mDialPaint?.color = resources.getColor(mFanOffColor,null);
            }
            // Redraw the view.
            invalidate();
        }

        if (attrs != null) {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.FanControl,
                0, 0
            )
            // Set the fan on and fan off colors from the attribute values
            mFanOnColor = typedArray.getColor(
                R.styleable.FanControl_fanOnColor,
                mFanOnColor
//                resources.getColor(mFanOnColor,null)
            )
            mFanOffColor = typedArray.getColor(
                R.styleable.FanControl_fanOffColor,
                mFanOffColor
//                resources.getColor(mFanOffColor,null)
            )

            SELECTION_COUNT = typedArray.getInt(
                R.styleable.FanControl_fanMax,
                SELECTION_COUNT
            )
            typedArray.recycle()
        }

    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        // Calculate the radius from the width and height.
        mWidth = w.toFloat()
        mHeight = h.toFloat()
        mRadius = (Math.min(mWidth, mHeight) / 2 * 0.8).toFloat()
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawCircle(mWidth / 2, mHeight / 2, mRadius, mDialPaint!!)
        // Draw the text labels.
        // Draw the text labels.
        val labelRadius = mRadius + 20
        val label = mTempLabel
        for (i in 0 until SELECTION_COUNT) {
            val xyData = computeXYForPosition(i, labelRadius)
            val x = xyData!![0]
            val y = xyData!![1]
            label.setLength(0)
            label.append(i)
            canvas!!.drawText(label, 0, label.length, x, y, mTextPaint!!)
        }
        // Draw the indicator mark.
        // Draw the indicator mark.
        val markerRadius = mRadius - 50
//        val markerRadius = mRadius - 35
        val xyData = computeXYForPosition(
            mActiveSelection,
            markerRadius
        )
        val x = xyData!![0]
        val y = xyData!![1]
//        canvas!!.drawCircle(x, y, 30f, mTextPaint!!)
        canvas!!.drawCircle(x, y, 30f, mTextPaint!!)

    }

    private fun computeXYForPosition(pos: Int, radius: Float): FloatArray? {
        val result = mTempResult
        val startAngle = Math.PI * (9 / 8.0) // Angles are in radians.
        val angle = startAngle + pos * (Math.PI / 4)
        result[0] = (radius * Math.cos(angle)).toFloat() + mWidth / 2
        result[1] = (radius * Math.sin(angle)).toFloat() + mHeight / 2
        return result
    }

    fun getSelectionCount(): Int {
        return SELECTION_COUNT
    }

    fun setSelectionCount(count: Int) {
        this.SELECTION_COUNT = count
        invalidate()
    }
}