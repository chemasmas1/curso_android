package mx.chemasmas.cursos.databinding2

import android.app.Application
import androidx.room.Room
import mx.chemasmas.cursos.databinding2.services.CardsAPI
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import mx.chemasmas.cursos.databinding2.BuildConfig.BASE_URL
import mx.chemasmas.cursos.databinding2.room.HandDatabase
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        retrofit = Retrofit.Builder()
            .baseUrl( BASE_URL )
            .addConverterFactory(
                ScalarsConverterFactory.create()
            )
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        cliente = retrofit?.create(CardsAPI::class.java)

        db = Room.databaseBuilder(applicationContext,HandDatabase::class.java,"manos")
//            .allowMainThreadQueries() //TODO remover o evitar
            .fallbackToDestructiveMigration()
            .build()

    }

    companion object{
        var retrofit: Retrofit? = null
        val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()!!
        var cliente: CardsAPI? = null
        lateinit var db:HandDatabase
    }

}