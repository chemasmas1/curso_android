package mx.chemasmas.cursos.sesion8

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import mx.chemasmas.cursos.sesion8.databinding.ActivityCapturaBinding
import java.io.File
import java.util.concurrent.Executors

class CapturaActivity : AppCompatActivity() {

    private var imageCapture: ImageCapture? = null
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>
    lateinit var binding:ActivityCapturaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_preview)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_captura)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()
                vincular(cameraProvider)
            },
            ContextCompat.getMainExecutor(this)
        )

        binding.floatingActionButton.setOnClickListener {
            takeFoto()
        }
    }

    @SuppressLint("RestrictedApi")
    private fun vincular(cameraProvider: ProcessCameraProvider?) {
        var preview = Preview.Builder()
            .setBackgroundExecutor(
                Executors.newSingleThreadExecutor()
            )
            .build()

        val camerSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()

        preview.setSurfaceProvider(
            binding.previewView.surfaceProvider
        )

        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
            .setTargetAspectRatio(AspectRatio.RATIO_4_3)
            .setTargetRotation(
                binding.previewView.display.rotation
            )
            .build()

        val camera = cameraProvider?.bindToLifecycle(this as LifecycleOwner,camerSelector,preview,imageCapture)
    }

    fun takeFoto(){
        val imageCapture = imageCapture ?: return




        val photoFile = File(
            dataDir,"foto.jpg"
        )
        photoFile.createNewFile()
        val outputOptions = ImageCapture.OutputFileOptions.Builder(
            photoFile
        )
        .build()

        imageCapture.takePicture(
            ContextCompat.getMainExecutor(this),
            FotoCaptura(binding.imageView, binding.previewView )
        )

        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            FotoGuardado()
        )
    }
}

class FotoCaptura(val imageView: ImageView, val previewView: PreviewView) :ImageCapture.OnImageCapturedCallback(){
    override fun onCaptureSuccess(image: ImageProxy) {
        imageView.setImageBitmap(
            previewView.bitmap
        )

        image.close()
    }

    override fun onError(exception: ImageCaptureException) {
        super.onError(exception)
    }
}

class FotoGuardado : ImageCapture.OnImageSavedCallback {
    override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
        Log.d("save",outputFileResults.toString())
        Log.d("save",outputFileResults.savedUri.toString())

    }

    override fun onError(exception: ImageCaptureException) {
        Log.d("save",exception.message.toString())
    }
}