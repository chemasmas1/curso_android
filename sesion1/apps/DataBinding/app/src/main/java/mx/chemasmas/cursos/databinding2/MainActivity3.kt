package mx.chemasmas.cursos.databinding2

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mx.chemasmas.cursos.databinding2.databinding.ActivityMain3Binding

class MainActivity3 : AppCompatActivity() {


    var stepper:AnimationDrawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main3)
        val binding: ActivityMain3Binding = DataBindingUtil.setContentView(this,R.layout.activity_main3)

        GlobalScope.launch {
            repeat(100){
                delay(500)
                binding.clearableEditText.rotation = (45 * it).toFloat()

            }
        }

        binding.imageView3.setBackgroundResource(R.drawable.my_anim)
        stepper = binding.imageView3.background as AnimationDrawable

        binding.imageView3.setOnTouchListener { v, event ->
            if(event.action == MotionEvent.ACTION_DOWN){
                stepper?.start()
            }
            super.onTouchEvent(event)
        }
    }

}