# Sensors

Sensores integrados que miden el movimiento , la orientacion y condiciones ambientales.

Se clasifican en

* De movimiento
    * acelerometros
    * sensores de gravedad
    * giroscopios
    * Sensores del vector de rotacion
* Ambientales
    * barometros
    * fotometros
    * termometros
* De posicion
    * sensores de orientacion
    * magnetometros

| Sensor |Tipo | Descripcion | Usos habituales
---|---|---|---
| TYPE_ACCELEROMETER | Hardware | m/s<sup>2</sup> aceleracion (x,y,z) | Deteccion del movimiento |
| TYPE_AMBIENT_TEMPERATURE | Hardware | <sup>o</sup>C | Temperatura del aire |
| TYPE_GRAVITY | Hardware o Software | m/s<sup>2</sup> accion de la gravedad (x,y,z) | Deteccion de movimiento |
| TYPE_GYROSCOPE | Hardware | rad/s rotacion del dispositivo (x,y,z) | Deteccion de rotacion |
| TYPE_LIGHT | Hardware | Luz ambiental Lx | Temperatura del aire |




## Sensor Manager

sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

sensorManager.getSensorList(Sensor.TYPE_ALL)

## SensorEventListener 
Se debe registrar en el resume
y eliminar el registro en el on pause


##  Buenas Practicas de Sensores

* se debe de pedir los datos en segundo plano

En los dispositivos que ejecutan Android 9 (API nivel 28) o posterior, las aplicaciones que se ejecutan en segundo plano tienen las siguientes restricciones:

Los sensores que usan el modo de generación de informes continuo, como acelerómetros y giroscopios, no reciben eventos.
Los sensores que utilizan los modos de generación de informes cuando se producen cambios o por única vez no reciben eventos.
Teniendo en cuenta estas restricciones, es mejor detectar los eventos del sensor cuando la app está en primer plano o como parte de un servicio en primer plano.

* Se debe cancelar el registro SIEMPRE
* Evitar Bloquear SensorChanged
* Evitar usar sensores obsoletos
    * TYPE_ORIENTATION , usar getOrientation
    * TYPE_TEMPERATURE , usar TYPE_AMBIENT_TEMPERATURE


# Ubicacion

## Permisos 

Para definir los permisos necesarios de nuestra app, Se categorizan en
* primer plano
* segundo plano

### Primer plano

Ejemplos de un app en primer plano

* Un app de navegacion para obtene instrucciones a otro lugar
* Una app de mensajeria que comparte la ubicacion actual de usuario.

Se consifera una app en primer plano si se cumple alguna de estas condiciones

* Un activity de la app que siempre es visible
* La app ejecuta un servicio en primer plano
    * el servicio debe de ser location

#### Permisos requeridos
Los permisos requeridos para usar la localizacion en android son los siguientes

```xml
<manifest ... >
  <!-- To request foreground location access, declare one of these permissions. -->
  <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
  <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
</manifest>
```

#### Coarse Location
Precision de ubicacion con hasta una cuadra de precision

#### FINE LOCATION
Da una ubicacion mas precisa. Es obligatorio al usar Dispositivos BLE

Se requiere realizar la siguiente declaracion de servicio para acceder al segundo plano. Est varia segun la version de android
```xml
<!-- Recommended for Android 9 (API level 28) and lower. -->
<!-- Required for Android 10 (API level 29) and higher. -->
<service
    android:name="MyNavigationService"
    android:foregroundServiceType="location" ... >
    <!-- Any inner elements would go here. -->
</service>
```

### Segundo Plano

Ejemplos de un app con location en segundo plano

* Un ap que requiere actualizar la ubicacion de un usuario  con otros.
* El api de Geovallado


### Permiso en segundo plano

Se requiere ademas de los permisos de location, se requiere agregar el servicio de segundo plano.

```xml
<manifest ... >
  <!-- Required only when requesting background location access on
       Android 10 (API level 29) and higher. -->
  <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
</manifest>
```

Parte de las nuevas politicas no sexige solicitar el acceso al permiso en tiempo de ejecucion.

Se recomienda que se haga una solicitud incremental, iniciando por el permiso de ubicacion en primer plano, y posteriormente escalarlo a segundo plano.

Apartir de android 10 (API 29) se agrega a las opciones de seleccion, la opcion Permiir todo el tiempo, si esta es seleccionada, le dara los permisos de segundo plano.

En android 11 (API 30) esta opcion desaparece en pos de una pagina de configuracion donde debe autorizarla.

Cuando el permiso falla, se debe de mostrar una UI que incluya lo siguiente:

* Una explicacion de por que se requeire el acceso a laubicacion en segundo plano
* una etiqueta visible para el usuario que muestra que se esta usando el permiso
* Una opcion para que los usuarios puedan rechazar el permiso y no debe de bloquear el uso de la app

