package mx.chemasmas.cursos.myanimations.models

import mx.chemasmas.cursos.myanimations.models.Card


/*
{
    "success": true,
    "cards": [
        {
            "image": "https://deckofcardsapi.com/static/img/KH.png",
            "value": "KING",
            "suit": "HEARTS",
            "code": "KH"
        }
    ],
    "deck_id":"3p40paa87x90",
    "remaining": 50
}
 */
data class Draw(
    val success:Boolean,
    val cards:ArrayList<Card>,
    val deck_id:String,
    val remaining:Int
)
