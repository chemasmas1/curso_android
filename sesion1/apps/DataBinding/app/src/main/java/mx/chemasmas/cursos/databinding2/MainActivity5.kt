package mx.chemasmas.cursos.databinding2

import android.animation.ObjectAnimator
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mx.chemasmas.cursos.databinding2.databinding.ActivityMain5Binding
import mx.chemasmas.cursos.databinding2.recyclerview.CartasAdapter
import mx.chemasmas.cursos.databinding2.recyclerview.MyItemDetailsLookup
import mx.chemasmas.cursos.databinding2.recyclerview.MyKeyProvider
import mx.chemasmas.cursos.databinding2.recyclerview.OverlapDecoration
import mx.chemasmas.cursos.databinding2.services.CardService
import mx.chemasmas.cursos.databinding2.services.CardServiceImpl
import mx.chemasmas.cursos.databinding2.viewmodel.BaseViewModel

class MainActivity5 : AppCompatActivity() ,
    CardService by CardServiceImpl() {

    val modelo: BaseViewModel by viewModels()
    lateinit var adapter:CartasAdapter

    var tracker:SelectionTracker<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main5)

        val shrinkAnimator = ObjectAnimator.ofFloat(
            this,"radius",200f,0f
        )
        shrinkAnimator.duration = 2000L
        shrinkAnimator.interpolator = LinearInterpolator()
        shrinkAnimator.startDelay = 1000L

        val binding: ActivityMain5Binding = DataBindingUtil.setContentView(this,R.layout.activity_main5)  // Para uso con Data Binding
        binding.lifecycleOwner = this

        adapter = CartasAdapter(modelo.mano)

        binding.cartas.adapter = adapter
        binding.cartas.addItemDecoration(OverlapDecoration())
        binding.cartas.itemAnimator = object: RecyclerView.ItemAnimator() {
            override fun animateDisappearance(
                viewHolder: RecyclerView.ViewHolder,
                preLayoutInfo: ItemHolderInfo,
                postLayoutInfo: ItemHolderInfo?
            ): Boolean {
                Log.d("rva","disapareeance")
                return true
            }

            override fun animateAppearance(
                viewHolder: RecyclerView.ViewHolder,
                preLayoutInfo: ItemHolderInfo?,
                postLayoutInfo: ItemHolderInfo
            ): Boolean {
                Log.d("rva","appearance")
                return true
            }

            override fun animatePersistence(
                viewHolder: RecyclerView.ViewHolder,
                preLayoutInfo: ItemHolderInfo,
                postLayoutInfo: ItemHolderInfo
            ): Boolean {
                Log.d("rva","persisntance")
                return true
            }

            override fun animateChange(
                oldHolder: RecyclerView.ViewHolder,
                newHolder: RecyclerView.ViewHolder,
                preLayoutInfo: ItemHolderInfo,
                postLayoutInfo: ItemHolderInfo
            ): Boolean {
                Log.d("rva","change")
                return true
            }

            override fun runPendingAnimations() {
                Log.d("rva","run pending")

            }

            override fun endAnimation(item: RecyclerView.ViewHolder) {
                Log.d("rva","end")

            }

            override fun endAnimations() {
                Log.d("rva","end ALL")

            }

            override fun isRunning(): Boolean {
                Log.d("rv","running")
                return true
            }
        }
//        binding.cartas.addOnItemTouchListener(
//            object : RecyclerView.OnItemTouchListener{
//
//
//                override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
//                    val child = rv.findChildViewUnder(e.x,e.y)!!
//                    val pos = rv.getChildAdapterPosition(child)
//
//                    Toast.makeText(child.context, modelo.mano.value?.get(pos)?.code ?: "",Toast.LENGTH_SHORT).show()
//                    return false
//                }
//
//                override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
//
//                }
//
//                override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
//
//                }
//
//            }
//        )

        modelo.draw.observe(this){
            if(it == null) return@observe
            Glide.with(this).load(it.cards[0].image).into(binding.lastcard)
            modelo.mano.value?.add(it.cards[0])
            adapter.notifyDataSetChanged()


        }


        tracker = SelectionTracker.Builder<String>(
            "mySelection",
            binding.cartas,
            MyKeyProvider(binding.cartas.adapter as CartasAdapter),
            MyItemDetailsLookup(binding.cartas),
            StorageStrategy.createStringStorage()
        )
//        .withSelectionPredicate(
//            SelectionPredicates.createSelectAnything()
//        )
        .build()

        adapter.tracker = tracker

    }

//    fun nuevoDeck(view: View){
//        nuevoDeck(modelo)
//    }

    fun robarCarta(view: View){
        drawOneCard(modelo.deck.value?.deck_id ?: "", modelo)
    }

    companion object{
        @JvmStatic
        @BindingAdapter("imgC")
        fun imgC(view: ImageView, url:String?){
            url?.let{
                Glide.with(view.context).load(url).into(view)
            }
        }
    }

    fun setRadius(radius: Float) {
//        mPaint.color = Color.GREEN + radius.toInt() / COLOR_ADJUSTER
//        invalidate()
    }
}